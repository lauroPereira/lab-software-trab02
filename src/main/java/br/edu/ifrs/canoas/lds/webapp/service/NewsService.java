package br.edu.ifrs.canoas.lds.webapp.service;

import org.springframework.stereotype.Service;

import br.edu.ifrs.canoas.lds.webapp.domain.News;
import br.edu.ifrs.canoas.lds.webapp.repository.NewsRepository;
import java.util.List;

/*
 *  Create by Edward Ramos Sep/15/2017
 *  
 */
@Service
public class NewsService {

    private final NewsRepository newsRepository;

    public NewsService(NewsRepository newsRepository) {
        this.newsRepository = newsRepository;
    }

    public News save(News news) {
        return newsRepository.save(news);
    }

    public News getOne(News news) {
        return newsRepository.findById(news.getId()).get();
    }

    public News getId(Long id) {
        return newsRepository.getOne(id);
    }

    public boolean delete(Long id) {

        try {
            newsRepository.deleteById(id);
            return true;

        } catch (Exception e) {
            return false;
        }
    }

    public Iterable<News> list() {
        return newsRepository.findAll();
    }

    public boolean isExist(Long id) {
        return newsRepository.existsById(id);
    }
    
    public List<News> findLastestNews(){
        return newsRepository.findTop2ByOrderByDataPublicationDesc();
    }
}
