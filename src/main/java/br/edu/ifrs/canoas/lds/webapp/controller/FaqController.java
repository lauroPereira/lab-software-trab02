package br.edu.ifrs.canoas.lds.webapp.controller;


import java.io.IOException;
import java.util.Locale;

import javax.validation.Valid;

import br.edu.ifrs.canoas.lds.webapp.config.auth.UserImpl;
import org.springframework.http.HttpEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.edu.ifrs.canoas.lds.webapp.config.Messages;
import br.edu.ifrs.canoas.lds.webapp.domain.Faq;
import br.edu.ifrs.canoas.lds.webapp.service.FaqService;

@Controller
@RequestMapping("/faq/")
public class FaqController {

    private final Messages messages;
    private final FaqService faqService;

    public FaqController(Messages messages, FaqService faqService) {
        this.messages = messages;
        this.faqService = faqService;
    }

    @GetMapping("/")
    public ModelAndView home() {
        return new ModelAndView("/index");
    }

    @GetMapping("list")
    public ModelAndView list(Model model) {
        model.addAttribute("faqs", faqService.list());
        System.out.println(faqService.list().toString());
        return new ModelAndView("/index");
    }

    @Secured("ROLE_ADMIN")
    @GetMapping("aprove")
    public ModelAndView aprove(Model model) {
        model.addAttribute("faqs", faqService.findAllByStatus());
        System.out.println(faqService.list().toString());
        return new ModelAndView("/faq/aprove");
    }

    @GetMapping("newquestion")
    public ModelAndView newquestion(Model model) {
        Faq faq = new Faq();

        model.addAttribute("faq", faq);
        model.addAttribute("categorys", faqService.listAllCategory());
        return new ModelAndView("/faq/newquestion");
    }

    @GetMapping("question")
    public ModelAndView question(Model model) {
        Faq faq = new Faq();

        model.addAttribute("faq", faq);
        model.addAttribute("categorys", faqService.listAllCategory());
        return new ModelAndView("/faq/newquestion");
    }

    @GetMapping("download/{id}")
    @ResponseBody
    public HttpEntity<byte[]> download(Model model, @PathVariable Long id) {
        return faqService.fileDownload(id);
    }
    
    @GetMapping("like/{id}")
    public String like (@PathVariable Long id){
        faqService.like(id);
        return "redirect:/";
    }
    
    @GetMapping("unlike/{id}")
    public String unlike (@PathVariable Long id){
        faqService.unlike(id);
        return "redirect:/";
    }


    @GetMapping("status/{id}")
    public String aprove (@PathVariable Long id){
        faqService.status(id);
        return "redirect:/";
    }

    @PostMapping("submit")
    public ModelAndView submit(@Valid Faq faq, @RequestParam("myFile") MultipartFile myFile, BindingResult bindingResult, RedirectAttributes redirectAttr, Locale locale, @AuthenticationPrincipal UserImpl activeUser) throws IOException {
        if (bindingResult.hasErrors()) {
            return new ModelAndView("/index");
        } else {
            Faq savedFaq = faqService.save(faq, myFile);
            if (savedFaq==null) redirectAttr.addFlashAttribute("message", messages.get("field.invalidformat"));
            //faqService.sendMessage(faq, activeUser.getUser());
            else redirectAttr.addFlashAttribute("message", messages.get("field.saved"));
        }
        return new ModelAndView("redirect:/faq/question");
    }
}
