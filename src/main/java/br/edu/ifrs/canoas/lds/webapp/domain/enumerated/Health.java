package br.edu.ifrs.canoas.lds.webapp.domain.enumerated;

public enum Health {
    BULL("Mais forte que um touro"),
    RETIRED("Um touro aposentado talvez?"),
    NOTBULL("Definitivamente não é um touro"),
    GOOD("Seus melhores dias já se foram. Mas ainda tem um bom coração");

    public String health;

    Health(String health) {
        this.health = health;
    }

    public String getHealth() {
        return health;
    }
}
