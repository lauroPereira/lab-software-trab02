/*
 * Copyright (c) 2017. Lauro Pereira
 */

package br.edu.ifrs.canoas.lds.webapp.controller;

import br.edu.ifrs.canoas.lds.webapp.config.Messages;
import br.edu.ifrs.canoas.lds.webapp.domain.Ad;
import br.edu.ifrs.canoas.lds.webapp.service.AdService;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.io.IOException;
import java.util.Date;

@Controller
@RequestMapping("/ads/")
public class AdController {
	private final AdService adService;
	private final Messages messages;

	public AdController(AdService adService, Messages messages){
		this.adService=adService;
		this.messages=messages;
	}
	
	@GetMapping("new")
	public ModelAndView newAd(Model model) {

		ModelAndView mav = new ModelAndView("/ads/new");
		mav.addObject("ad", new Ad());
		return mav;
	}

	@PostMapping("submit")
	public ModelAndView submit(@Valid Ad ad, @RequestParam("myImage") MultipartFile image, BindingResult bindingResult, RedirectAttributes redirectAttr) throws IOException {
		if (bindingResult.hasErrors()) {
			return new ModelAndView("/index");
		} else {
			adService.save(ad, image);
			redirectAttr.addFlashAttribute("message", messages.get("field.saved"));
		}
		return new ModelAndView("redirect:/ads/new");
	}

	@Secured("ROLE_ADMIN")
	@GetMapping("list")
	public ModelAndView list() {
		return new ModelAndView("/ads/list");
	}

}