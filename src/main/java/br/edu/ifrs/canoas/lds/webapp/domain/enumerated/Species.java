package br.edu.ifrs.canoas.lds.webapp.domain.enumerated;

public enum Species {
    DOG("É um totó"),
    CAT("Faz 'miau'"),
    OTHER("Não é totó e nem faz 'miau'");

    public String valor;

    Species(String species) {
        this.valor = species;
    }

    public String getValor() {
        return valor;
    }


}
