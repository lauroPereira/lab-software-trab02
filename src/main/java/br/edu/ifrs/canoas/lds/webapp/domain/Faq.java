package br.edu.ifrs.canoas.lds.webapp.domain;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;

@Entity
public class Faq {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @NotEmpty
    private String question;

    @NotEmpty
    private String answer;

    @ManyToOne
    private Category category;

    private Long likes;
    private Long unlikes;

    @OneToOne
    private File file;

    private boolean status;

    public Faq(){

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Long getUnlikes() {
        return unlikes;
    }

    public void setUnlikes(Long unlikes) {
        this.unlikes = unlikes;
    }

    public Long getLikes() {
        return likes;
    }

    public void setLikes(Long likes) {
        this.likes = likes;
    }

    public Long getUnLikes() {
        return unlikes;
    }

    public void setUnLikes(Long unlikes) {
        this.unlikes = unlikes;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }
}
