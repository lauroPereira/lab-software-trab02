package br.edu.ifrs.canoas.lds.webapp.domain;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

/*
 *  Create by Edward Ramos Sep/15/2017
 *  "Notícias" in Portuguese
 */

@Entity
public class News {

	@Id
	@GeneratedValue
	private Long id;
	// TODO #60 Issue
	//@NotNull(message="{field.required}") @NotEmpty(message="{field.empty}")
	private String author;
	@NotNull(message="{field.required}") @NotEmpty(message="{field.empty}")
	private String title;
	@NotNull(message="{field.required}") @NotEmpty(message="{field.empty}")
	private String caption;
	@NotNull(message="{field.required}") @NotEmpty(message="{field.empty}")
	private String description;
	
	@OneToOne
	private File photo;
	
	@DateTimeFormat(iso= DateTimeFormat.ISO.DATE)
	private Date dataPublication;
	
	public News(){}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCaption() {
		return caption;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public File getPhoto() {
		return photo;
	}

	public void setPhoto(File photo) {
		this.photo = photo;
	}

	public Date getDataPublication() {
		return dataPublication;
	}

	public void setDataPublication(Date dataPublication) {
		this.dataPublication = dataPublication;
	}
	
}
