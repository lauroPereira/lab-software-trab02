package br.edu.ifrs.canoas.lds.webapp.controller;

import br.edu.ifrs.canoas.lds.webapp.config.Messages;
import br.edu.ifrs.canoas.lds.webapp.config.auth.UserImpl;
import br.edu.ifrs.canoas.lds.webapp.domain.Pet;
import br.edu.ifrs.canoas.lds.webapp.domain.filter.PetSearch;
import br.edu.ifrs.canoas.lds.webapp.service.PetService;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Set;

@Controller
@RequestMapping("/pet/")
public class PetController {
	private final PetService petService;
	private final Messages messages;
	
	
	public PetController(Messages messages, PetService petService){
		this.messages=messages;
		this.petService=petService;
	}
	
	@GetMapping("new")
	public ModelAndView newAnimal(Model model, @AuthenticationPrincipal UserImpl user) {
		model.addAttribute("pet", new Pet());
		return new ModelAndView("/pet/form");
	}

	/*@GetMapping("save")
	public ModelAndView record(Model model, @AuthenticationPrincipal UserImpl user) {
		model.addAttribute("pet", new Pet());
		//model.addAttribute("registers",occurrenceService.findIncompleteRegister(user.getUser().getId()));
		//model.addAttribute("action", "record");
		return new ModelAndView("/pet/list");
	}*/

	@PostMapping("save")
	public ModelAndView save(@ModelAttribute("pet")@Valid Pet pet, BindingResult bindingResult, RedirectAttributes redirectAttr, @AuthenticationPrincipal UserImpl activeUser,
							 @RequestParam("myFile") Set<MultipartFile> myFile, Errors errors, Model model) throws IOException {
		if(bindingResult.hasErrors()){
			redirectAttr.addFlashAttribute(errors);
			System.out.println(errors.toString());
			System.out.println("entrou no if");
			return new ModelAndView("/pet/list");
		}
		System.out.println("passou reto");
		petService.save(pet, activeUser.getUser(), myFile);
		petService.sendMessage(pet, activeUser.getUser());
		redirectAttr.addFlashAttribute("message", messages.get("field.saved"));
		return new ModelAndView("redirect:/pet/listAll");
	}
	
	@GetMapping("listuser")
	public ModelAndView listUser(Model model, @AuthenticationPrincipal UserImpl activeUser) {
        model.addAttribute("pets", petService.findAllByUserId(activeUser.getUser().getId()));
		return new ModelAndView("/pet/list");
	}


	@GetMapping("listAll")
	public ModelAndView listAll(Model model) {
        model.addAttribute("pets", petService.findAllByStatus());
		return new ModelAndView("/pet/list");
	}

	@GetMapping("listFiltered")
	public ModelAndView list(@ModelAttribute("petSearch") PetSearch petSearch, Model model) {
		model.addAttribute("pets", petService.findBySearch(petSearch));
		return new ModelAndView("/pet/list");
	}
	
	@GetMapping("view/{id}")
	public ModelAndView view(Model model, @PathVariable Long id){
		model.addAttribute("pet", petService.getOne(id));
		model.addAttribute("pictures",petService.getPicturesOfPet(id));
		return new ModelAndView("/pet/view");	
	}
	
	@GetMapping("search")
	public ModelAndView search(Model model){
		model.addAttribute("petSearch",new PetSearch());
	    return new ModelAndView("/pet/search");
	}

	@GetMapping("/adopt/{id}")
	public ModelAndView adopt(Model model, @PathVariable Long id,RedirectAttributes redirectAttr){
		model.addAttribute("pet",petService.adopt(id));
		redirectAttr.addFlashAttribute("pet", petService.adopt(id));
		return new ModelAndView("redirect:/pet/view/" + id);
	}
}
