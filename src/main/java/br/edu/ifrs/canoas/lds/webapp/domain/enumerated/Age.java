package br.edu.ifrs.canoas.lds.webapp.domain.enumerated;

public enum Age {
    BABY("Bebê (0-6 meses)"),
    ADOLESCENT("Adolescente (1-4 anos)"),
    ADULT("Adulto (4-8 anos)"),
    ANCIENT("Ancião (+8 anos)");

    public String age;

    Age(String age) {
        this.age = age;
    }

    public String getAge() {
        return age;
    }
}
