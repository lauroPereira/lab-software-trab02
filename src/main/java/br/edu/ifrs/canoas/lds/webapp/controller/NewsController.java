package br.edu.ifrs.canoas.lds.webapp.controller;

import java.util.Locale;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.edu.ifrs.canoas.lds.webapp.config.Messages;
import br.edu.ifrs.canoas.lds.webapp.domain.News;
import br.edu.ifrs.canoas.lds.webapp.service.NewsService;

/*
 *  Create by Edward Ramos Sep/13/2017
 *  
 */
@Controller
@RequestMapping("/news/")
public class NewsController {

	private final Messages messages;
	private final NewsService newsService;
	
	public NewsController(Messages messages, NewsService newsService) {
		this.messages = messages;
		this.newsService = newsService;
	}

	@GetMapping("/list")
	public ModelAndView list(){
		
		// TODO #64 Issues
		ModelAndView mav = new ModelAndView("/news/list");		
		mav.addObject("n", newsService.list());
		return mav;
	}
	
	@GetMapping("/view/{id}")
	public ModelAndView view(@PathVariable Long id, RedirectAttributes redirectAttr){
		
		//TODO #67 Issues
		if(newsService.isExist(id)==true){
			ModelAndView mav = new ModelAndView("/news/view");
			mav.addObject("isView", true); //true = No editable fields
			return mav;	
		}else{
			ModelAndView mav2 = new ModelAndView("redirect:/news/list");
			redirectAttr.addFlashAttribute("message", messages.get("news.idNotFound"));
			return mav2;
		}
		
	}
	
	@GetMapping("/test")
	public ModelAndView test(){
		ModelAndView mav = new ModelAndView("/news/view");
		return mav;
	}
	
	@GetMapping("/create")
	public ModelAndView create(){
		
		ModelAndView mav = new ModelAndView("/news/new");
		
		mav.addObject("news", new News());
		mav.addObject("isCreate", true); //false = editable fields
		return mav;
	}
	
	@GetMapping("/edit/{id}")
	public ModelAndView edit(@PathVariable("id") Long id, RedirectAttributes redirectAttr){
		
		//TODO #67 Issues
		if(newsService.isExist(id)==true){
			ModelAndView mav = new ModelAndView("/news/view");
			mav.addObject("news", newsService.getId(id));
			mav.addObject("isEdit", true); //false = editable fields
			return mav;
		}else{
			ModelAndView mav2 = new ModelAndView("redirect:/news/list");
			redirectAttr.addFlashAttribute("message", messages.get("news.idNotFound"));
			return mav2;
		}
		
	}
	
	@GetMapping("/delete/{id}")
	public ModelAndView delete(@PathVariable("id") Long id, RedirectAttributes redirectAttr){
		ModelAndView mav = new ModelAndView("redirect:/news/list");
		
		
		if(newsService.delete(id)==false){
			redirectAttr.addFlashAttribute("message", messages.get("news.idNotFound"));
		}else{
			redirectAttr.addFlashAttribute("message", messages.get("field.deleted"));
		}
		
		return mav;
	}
	
	@PostMapping("/save")
	public ModelAndView save(@Valid News news, BindingResult bindingResult, 
            RedirectAttributes redirectAttr, Locale locale){
		
		if (bindingResult.hasErrors()) {
			ModelAndView mav = new ModelAndView("/news/new");
			mav.addObject("isCreate", true);
            return mav;
            
        }
		
		ModelAndView mav = new ModelAndView("redirect:/news/list");
		mav.addObject("news", newsService.save(news));
		redirectAttr.addFlashAttribute("message", messages.get("field.saved"));
		
		return mav;
	}
}
