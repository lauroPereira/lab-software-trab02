package br.edu.ifrs.canoas.lds.webapp.domain;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.sun.org.apache.xpath.internal.operations.Mult;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

import br.edu.ifrs.canoas.lds.webapp.domain.enumerated.Age;
import br.edu.ifrs.canoas.lds.webapp.domain.enumerated.Breed;
import br.edu.ifrs.canoas.lds.webapp.domain.enumerated.Gender;
import br.edu.ifrs.canoas.lds.webapp.domain.enumerated.Health;
import br.edu.ifrs.canoas.lds.webapp.domain.enumerated.Pelage;
import br.edu.ifrs.canoas.lds.webapp.domain.enumerated.Size;
import br.edu.ifrs.canoas.lds.webapp.domain.enumerated.Species;
import br.edu.ifrs.canoas.lds.webapp.domain.enumerated.Status;
import br.edu.ifrs.canoas.lds.webapp.domain.enumerated.Temper;
import org.springframework.web.multipart.MultipartFile;

/*
 *  Create by Andressa Job 17/09/2017
 */

@Entity
//@Table(uniqueConstraints=@UniqueConstraint(columnNames="NAME")) first unique validation method
public class Pet {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    
    
    @NotEmpty
    private String name;
    @NotEmpty
    private String description;
    //private String feedback;
    //private String justification;
    @NotEmpty
    private String localization;
    @DateTimeFormat(iso= DateTimeFormat.ISO.DATE)
    private Date expiration;
    private Date register;
    @OneToMany
	private Set<File> pictures;
    @Enumerated(EnumType.STRING)
    private Age age;
    @Enumerated(EnumType.STRING)
    private Breed breed;
    @Enumerated(EnumType.STRING)
    private Health health;
    @Enumerated(EnumType.STRING)
    private Status status;
    @Enumerated(EnumType.STRING)
    private Species species;
    @Enumerated(EnumType.STRING)
    private Gender gender;
    @Enumerated(EnumType.STRING)
    private Size size;
    @Enumerated(EnumType.STRING)
    private Pelage pelage;
    @Enumerated(EnumType.STRING)
    private Temper temperament;
    
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;
    

    public Pet() {
        pictures = new HashSet<File>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    /*public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }*/

    /*public String getJustification() {
        return justification;
    }

    public void setJustification(String justification) {
        this.justification = justification;
    }*/

    public Date getExpiration() {
        return expiration;
    }

    public void setExpiration(Date expiration) {
        this.expiration = expiration;
    }


    public Date getRegister() {
        return register;
    }

    public void setRegister(Date register) {
        this.register = register;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }


    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Size getSize() {
        return size;
    }

    public void setSize(Size size) {
        this.size = size;
    }

    public Pelage getPelage() {
        return pelage;
    }

    public void setPelage(Pelage pelage) {
        this.pelage = pelage;
    }

    public String getLocalization() {
        return localization;
    }

    public void setLocalization(String localization) {
        this.localization = localization;
    }

    public Temper getTemperament() {
        return temperament;
    }

    public void setTemperament(Temper temperament) {
        this.temperament = temperament;
    }

    public Set<File> getPictures() {
        return pictures;
    }

    //@Resource(name="pictures")
    public void setPictures(Set<File> pictures) {
        this.pictures = pictures;
    }

    //option to set pictures separated into pet
    public void setPicturesSeparated(File picture) {
        this.pictures.add(picture);
    }

    public Age getAge() {
        return age;
    }

    public void setAge(Age age) {
        this.age = age;
    }

    public Breed getBreed() {
        return breed;
    }

    public void setBreed(Breed breed) {
        this.breed = breed;
    }

    public Health getHealth() {
        return health;
    }

    public void setHealth(Health health) {
        this.health = health;
    }

    public Species getSpecies() {
        return species;
    }

    public void setSpecies(Species species) {
        this.species = species;
    }

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

    @Override
    public String toString() {
        return "Pet{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", localization='" + localization + '\'' +
                ", expiration=" + expiration +
                ", register=" + register +
                ", age=" + age +
                ", breed=" + breed +
                ", health=" + health +
                ", status=" + status +
                ", species=" + species +
                ", gender=" + gender +
                ", size=" + size +
                ", pelage=" + pelage +
                ", temperament=" + temperament +
                ", user=" + user +
                '}';
    }
}
