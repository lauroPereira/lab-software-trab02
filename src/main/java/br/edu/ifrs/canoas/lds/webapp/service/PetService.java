package br.edu.ifrs.canoas.lds.webapp.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import br.edu.ifrs.canoas.lds.webapp.domain.enumerated.*;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import br.edu.ifrs.canoas.lds.webapp.domain.File;
import br.edu.ifrs.canoas.lds.webapp.domain.Pet;
import br.edu.ifrs.canoas.lds.webapp.domain.User;
import br.edu.ifrs.canoas.lds.webapp.domain.filter.PetSearch;
import br.edu.ifrs.canoas.lds.webapp.repository.FileRepository;
import br.edu.ifrs.canoas.lds.webapp.repository.PetRepository;
import br.edu.ifrs.canoas.lds.webapp.repository.UserRepository;


//import org.springframework.web.multipart.MultipartFile;
//import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;

@Service
public class PetService {


    private final PetRepository petRepository;
    private final UserRepository userRepository;
    private final FileRepository fileRepository;
    private final JavaMailSender javaMailSender;

    public PetService(PetRepository petRepository, UserRepository userRepository, FileRepository fileRepository, JavaMailSender javaMailSender) {
        this.userRepository = userRepository;
        this.petRepository = petRepository;
        this.fileRepository = fileRepository;
        this.javaMailSender = javaMailSender;
    }


    public Pet getOne(Long id) {
        return petRepository.findById(id).get();
    }

    public List<Pet> findAllByStatus() {
        return petRepository.findAllByStatus(Status.WAITING);
    }

    public List<Pet> findAllByUserId(Long id) {
        return petRepository.getAllByUserId(userRepository.getOne(id).getId());
    }

    public Pet save(Pet pet, User user, Set<MultipartFile> files) throws IOException {
        Pet pet1 = new Pet();
        Set<File> fileArrayList = new HashSet<>();
        if (files != null) {
            for (MultipartFile oneFile : files) {
                if (oneFile.getBytes().length < 2147483647) {
                    File file = new File();
                    file.setContent(oneFile.getBytes());
                    file.setFilename(oneFile.getOriginalFilename());
                    file.setContentType(oneFile.getContentType());
                    if (file.getContentType().matches("image/jpg") || file.getContentType().matches("image/png")) {
                        System.out.println(file.getContentType());
                        fileArrayList.add(file);
                    }


                }
            }
        }

        // unique name validation method
        /*ArrayList<Pet> petlist = (ArrayList<Pet>) findAllByUserId(user.getId());
        boolean samePetName = false;
        if (!petlist.isEmpty()) {
            for (Pet pet2 : petlist) {
                if (pet2.getName().toString().equals(pet.getName().toString())) {
                    System.out.println("passei");
                    samePetName = true;
                }
                System.out.println("pet2 name = " + pet2.getName().toString());
                System.out.println("pet name = " + pet.getName().toString());
                System.out.println(samePetName);
            }
        }*/

        //if (!samePetName) {
        pet1.setName(pet.getName());
        pet1.setLocalization(pet.getLocalization());
        pet1.setGender(pet.getGender());
        pet1.setBreed(pet.getBreed());
        pet1.setDescription(pet.getDescription());
        pet1.setAge(pet.getAge());
        pet1.setHealth(pet.getHealth());
        pet1.setPelage(pet.getPelage());
        pet1.setSpecies(pet.getSpecies());
        pet1.setStatus(pet.getStatus());
        pet1.setExpiration(pet.getExpiration());
        pet1.setTemperament(pet.getTemperament());
        pet1.setSize(pet.getSize());
        pet1.setRegister(new Date());
        pet1.setUser(user);
        if (fileArrayList != null && fileArrayList.size() >= 2) {
            for (File file2 : fileArrayList) {
                file2 = fileRepository.save(file2);
            }
        } else {
            throw new IOException("No mínimo dois arquivos são necessários");
        }
        pet1.setPictures(fileArrayList);

        Pet petRet = new Pet();
        if (pet1.getName() != null &&
                pet1.getAge() != null &&
                pet1.getBreed() != null &&
                pet1.getDescription() != null &&
                pet1.getExpiration() != null &&
                pet1.getGender() != null &&
                pet1.getHealth() != null &&
                pet1.getLocalization() != null &&
                pet1.getPelage() != null &&
                pet1.getPictures() != null &&
                pet1.getSize() != null &&
                pet1.getSpecies() != null &&
                pet1.getTemperament() != null &&
                pet1.getUser() != null) {
            petRet = petRepository.save(pet1);

        }
        return petRet;


    }


    public void sendMessage(Pet pet, User user) {
        SimpleMailMessage mail = new SimpleMailMessage();

        if (pet != null || user != null) {
            if (user.getEmail() != null || user.getName() != null || pet.getName() != null) {
                if (user.getEmail().contains("@")) {
                    mail.setTo(user.getEmail());
                    mail.setFrom("petnationapp@gmail.com");
                    mail.setSubject("Seu pet foi cadastrado com sucesso!");
                    mail.setText(
                            "Prezado(a) " + user.getName() + ", \n\n" + "Seu pet de nome '"
                                    + pet.getName() + "' foi cadastrado com sucesso!\n");
                    javaMailSender.send(mail);
                }

            }
        }
    }

    public List<Pet> findBySearch(PetSearch search) {
        return petRepository.findAll(petSpecification(search));
    }

    public static Specification<Pet> petSpecification(PetSearch search) {
        return new Specification<Pet>() {
            @Override
            public Predicate toPredicate(Root<Pet> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                final Collection<Predicate> predicates = new ArrayList<>();
                String finalSearch = "";
                predicates.add(cb.equal(cb.upper(root.get("status")), Status.WAITING)); //pega somente os pets com status aguardando

                if (!StringUtils.isEmpty(search.getAdvancedSearch())) {
                    if (search.getAdvancedSearch().length() >= 3){
                    int countSpace = 0;
                    for (char ch : search.getAdvancedSearch().toCharArray()) {
                        if (ch == ' ') {
                            countSpace++;
                        }
                        if (countSpace < 2){
                            finalSearch += String.valueOf(ch);
                        }
                    }
                    predicates.add(
                            cb.or( //faz um or com o termo da pesquisa para pesquisar no nome e descrição do pet
                                    cb.like(cb.upper(root.get("name")), "%" + finalSearch.toUpperCase() + "%"),
                                    cb.like(cb.upper(root.get("description")), "%" + finalSearch.toUpperCase() + "%")));
                    }
                }

                if (search.isCat() || search.isDog() || search.isOther()) {
                    List<Species> species = new ArrayList<>();
                    if (search.isCat())
                        species.add(Species.CAT);
                    if (search.isDog())
                        species.add(Species.DOG);
                    if (search.isOther())
                        species.add(Species.OTHER);
                    predicates.add(root.get("species").in(species));
                }

                if (search.isMale() || search.isFemale()) {
                    List<Gender> genders = new ArrayList<>();
                    if (search.isMale())
                        genders.add(Gender.MALE);
                    if (search.isFemale())
                        genders.add(Gender.FEMALE);
                    predicates.add(root.get("gender").in(genders));
                }

                if (search.isSmall() || search.isMedium() || search.isBig() || search.isBigger()) {
                    List<Size> sizes = new ArrayList<>();
                    if (search.isSmall())
                        sizes.add(Size.SMALL);
                    if (search.isMedium())
                        sizes.add(Size.MEDIUM);
                    if (search.isBig())
                        sizes.add(Size.BIG);
                    if (search.isBigger())
                        sizes.add(Size.BIGGER);
                    predicates.add(root.get("size").in(sizes));
                }

                if (search.isBold() || search.isHairy() || search.isMhairy() || search.isVhairy()) {
                    List<Pelage> pelages = new ArrayList<>();
                    if (search.isBold())
                        pelages.add(Pelage.BOLD);
                    if (search.isHairy())
                        pelages.add(Pelage.HAIRY);
                    if (search.isMhairy())
                        pelages.add(Pelage.MHAIRY);
                    if (search.isVhairy())
                        pelages.add(Pelage.VHAIRY);
                    predicates.add(root.get("pelage").in(pelages));
                }

                if (search.isBull() || search.isNotBull() || search.isRetired() || search.isGood()) {
                    List<Health> healths = new ArrayList<>();
                    if (search.isBull())
                        healths.add(Health.BULL);
                    if (search.isNotBull())
                        healths.add(Health.NOTBULL);
                    if (search.isRetired())
                        healths.add(Health.RETIRED);
                    if (search.isGood())
                        healths.add(Health.GOOD);
                    predicates.add(root.get("health").in(healths));
                }

                if (search.isDetermined() || search.isUndetermined()) {
                    List<Breed> breeds = new ArrayList<>();
                    if (search.isDetermined())
                        breeds.add(Breed.DETERMINED);
                    if (search.isUndetermined())
                        breeds.add(Breed.UNDETERMINED);
                    predicates.add(root.get("breed").in(breeds));
                }

                if (search.isBaby() || search.isAdolescent() || search.isAdult() || search.isAncient()) {
                    List<Age> ages = new ArrayList<>();
                    if (search.isBaby())
                        ages.add(Age.BABY);
                    if (search.isAdolescent())
                        ages.add(Age.ADOLESCENT);
                    if (search.isAdult())
                        ages.add(Age.ADULT);
                    if (search.isAncient())
                        ages.add(Age.ANCIENT);
                    predicates.add(root.get("age").in(ages));
                }
                query.orderBy(cb.asc(root.get("localization")));
                return cb.and(predicates.toArray(new Predicate[predicates.size()])); //faz um and com todos os outros termos de busca
            }
        };
    }

    public Set<File> getPicturesOfPet(Long id) {
        return getOne(id).getPictures();
    }

    public List<Pet> findLatestPetsForAdoption() {
        return petRepository.findTop3ByStatusOrderByExpiration(Status.WAITING);
    }

    public Pet adopt(Long id) {
        Pet pet = petRepository.getOne(id);
        pet.setStatus(Status.ADOPTED);
        petRepository.save(pet);
        return pet;
    }
}
