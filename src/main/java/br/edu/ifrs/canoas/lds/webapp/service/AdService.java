/*
 * Copyright (c) 2017. Lauro Pereira
 */
package br.edu.ifrs.canoas.lds.webapp.service;

import br.edu.ifrs.canoas.lds.webapp.domain.Ad;
import br.edu.ifrs.canoas.lds.webapp.domain.File;
import br.edu.ifrs.canoas.lds.webapp.domain.enumerated.Plan;
import br.edu.ifrs.canoas.lds.webapp.repository.AdRepository;
import br.edu.ifrs.canoas.lds.webapp.repository.FileRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Service
public class AdService {

    private final AdRepository adRepository;
    private final FileRepository fileRepository;

    public AdService(AdRepository adRepository, FileRepository fileRepository) {
        this.adRepository = adRepository;
        this.fileRepository = fileRepository;
    }

    public Ad get(Long id) {
        return adRepository.findById(id).get();
    }

    public Ad save(Ad myAd, MultipartFile myFile) throws IOException {

        File file = new File();
        file.setContent(myFile.getBytes());
        file.setFilename(myFile.getOriginalFilename());
        file.setContentType(myFile.getContentType());

        if (myFile.getBytes().length <= 125000) {
            if (file.getContentType().matches("image/jpg")
                    || file.getContentType().matches("image/png")) {
                System.out.println(file.getContentType());
                fileRepository.save(file);
            }
        } else {
            throw new IOException("A imagem deve conter no maximo 1Mb.");
        }

        Ad ad = new Ad();
        ad.setTitle(myAd.getTitle());
        ad.setSubTitle(myAd.getSubTitle());
        ad.setContent(myAd.getContent());
        ad.setEmail(myAd.getEmail());
        ad.setPhone(myAd.getPhone());
        ad.setUrl(myAd.getUrl());
        ad.setInitDate(new Date());
        ad.setPlan(myAd.getPlan());
        ad.setVisitors(myAd.getVisitors());

        Calendar c = Calendar.getInstance();
        c.setTime(ad.getInitDate());
        switch (ad.getPlan().getId()) {
            case 1:
                c.set(Calendar.MONTH, c.get(Calendar.MONTH) + 1);
                ad.setExpDate(c.getTime());
                break;
            case 2:
                c.set(Calendar.MONTH, c.get(Calendar.MONTH) + 6);
                ad.setExpDate(c.getTime());
                break;
            case 3:
                c.set(Calendar.YEAR, c.get(Calendar.YEAR) + 1);
                ad.setExpDate(c.getTime());
                break;
        }
        ad.setImage(file);
        return adRepository.save(ad);
    }

    private List<Ad> getActive(){
        List<Ad> ads = adRepository.findAll();
        for (Ad ad:ads){
            if(ad.getExpDate().before(new Date())){
                ads.remove(ad);
            }
        }
        return ads;
    }

    public List<Ad> findShuffleAds() {
        List<Ad> ads = getActive();
        Collections.shuffle(ads);
        return getTop2Ads(ads);
    }

    private List<Ad> getTop2Ads(List<Ad> ads) {
        List<Ad> topAds = new ArrayList<Ad>();
        if (ads != null) {
            for (int i = 0; i < 2; i++) {
                topAds.add(ads.get(i));
            }
        }
        return topAds;
    }

    public String registryVisitor(Long adId){
        Ad ad = get(adId);

        ad.setVisitors(ad.getVisitors()+1);
        adRepository.save(ad);

        return ad.getUrl();
    }
}
