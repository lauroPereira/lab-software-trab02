package br.edu.ifrs.canoas.lds.webapp.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.edu.ifrs.canoas.lds.webapp.domain.News;
import java.util.List;

/*
 *  Create by Edward Ramos Sep/15/2017
 *  
 */
@Repository
public interface NewsRepository extends JpaRepository<News, Long>{

	Optional<News> findByTitle(String title);
        List<News> findTop2ByOrderByDataPublicationDesc();
}
