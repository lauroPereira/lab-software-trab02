package br.edu.ifrs.canoas.lds.webapp.repository;

import br.edu.ifrs.canoas.lds.webapp.domain.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * Created by rodrigo on 2/21/17.
 */
@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {

    Role findByRole(String role);
}