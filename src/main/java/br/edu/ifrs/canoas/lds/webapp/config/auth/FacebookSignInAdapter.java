package br.edu.ifrs.canoas.lds.webapp.config.auth;

import br.edu.ifrs.canoas.lds.webapp.service.UserDetailsImplService;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.web.SignInAdapter;
import org.springframework.web.context.request.NativeWebRequest;

public class FacebookSignInAdapter implements SignInAdapter {

    private final UserDetailsImplService userDetailsImplService;

    public FacebookSignInAdapter(UserDetailsImplService userDetailsImplService) {
        this.userDetailsImplService = userDetailsImplService;
    }

    @Override
    public String signIn(String localUserId, Connection<?> connection, NativeWebRequest request) {
        SecurityContextHolder.getContext().setAuthentication(
                new UsernamePasswordAuthenticationToken(
                        userDetailsImplService.loadUserByUsername(localUserId),
                        null,
                        AuthorityUtils.createAuthorityList("ROLE_FACEBOOK_USER")));

        return null;
    }
}