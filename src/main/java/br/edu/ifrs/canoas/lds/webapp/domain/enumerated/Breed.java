package br.edu.ifrs.canoas.lds.webapp.domain.enumerated;

public enum Breed {
    DETERMINED("Raça Determinada"),
    UNDETERMINED("Raça Indeterminada");

    public String breed;

    Breed (String breed) {
        this.breed = breed;
    }

    public String getBreed() {
        return breed;
    }
}
