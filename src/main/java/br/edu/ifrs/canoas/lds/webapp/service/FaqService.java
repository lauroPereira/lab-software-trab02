package br.edu.ifrs.canoas.lds.webapp.service;

import br.edu.ifrs.canoas.lds.webapp.domain.Category;
import br.edu.ifrs.canoas.lds.webapp.domain.Faq;
import br.edu.ifrs.canoas.lds.webapp.domain.File;
import br.edu.ifrs.canoas.lds.webapp.domain.User;
import br.edu.ifrs.canoas.lds.webapp.repository.CategoryRepository;
import br.edu.ifrs.canoas.lds.webapp.repository.FaqRepository;
import br.edu.ifrs.canoas.lds.webapp.repository.FileRepository;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

import static java.lang.Boolean.FALSE;

@Service
public class FaqService {

    private final FaqRepository faqRepository;
    private final CategoryRepository categoryRepository;
    private final FileRepository fileRepository;
    private final JavaMailSender javaMailSender;

    public FaqService(FaqRepository faqRepository, CategoryRepository categoryRepository, FileRepository fileRepository, JavaMailSender javaMailSender) {
        this.faqRepository = faqRepository;
        this.categoryRepository = categoryRepository;
        this.fileRepository = fileRepository;
        this.javaMailSender = javaMailSender;
    }

    public void sendMessage(Faq faq, User user) {
        SimpleMailMessage mail = new SimpleMailMessage();

        if (faq != null || user != null) {
            if(user.getEmail() != null || user.getName() != null || faq.getQuestion() != null || faq.getAnswer() != null) {
                if(user.getEmail().contains("@") ){
                    mail.setTo("petnationapp@gmail.com");
                    mail.setFrom("petnationapp@gmail.com");
                    mail.setSubject("Nova pergunta");
                    mail.setText(
                            "Prezado(a) Administrador " + ", \n\n" + "esta pergunta foi "
                                    + faq.getQuestion() + "' foi cadastrado no sistema!\n");
                    javaMailSender.send(mail);
                }

            }
        }
    }


    public Faq save (Faq faq, MultipartFile myFile) throws IOException {

        File file = new File();
        if (!myFile.isEmpty()){
            if (myFile.getOriginalFilename().split("\\.")[1].equals("pdf")){
                file.setContent(myFile.getBytes());
                file.setFilename(myFile.getOriginalFilename());
                file.setContentType(myFile.getContentType());
            }
            if (!myFile.getOriginalFilename().split("\\.")[1].equals("pdf")) return null;
        }

        fileRepository.save(file);
        /*if (myFile != null) {
            for (MultipartFile oneFile: myFile) {
                if(oneFile.getBytes().length < 2147483647) {
                    File files = new File();
                    file.setContent(oneFile.getBytes());
                    file.setFilename(oneFile.getOriginalFilename());
                    file.setContentType(oneFile.getContentType());
                    if(file.getContentType().matches("application/pdf")) {
                        System.out.println(file.getContentType());
                        fileRepository.save(file);
                    }
                }
            }
        }*/

        Faq faqs = new Faq();
        faqs.setQuestion(faq.getQuestion());
        faqs.setAnswer(faq.getAnswer());
        faqs.setCategory(faq.getCategory());
        faqs.setLikes((long) 0);
        faqs.setUnLikes((long)0);
        faqs.setStatus(FALSE);
        faqs.setFile(file);

        return faqRepository.save(faqs);

    }

    public List<Faq> list(){
        return faqRepository.findAll();
    }
    public List<Category> listAllCategory(){return categoryRepository.findAll();}
    public List<Faq> findAllByStatus() { return faqRepository.findAllByStatus(false); }
    public List<Faq> findAllByCategory(Long id) {
        return faqRepository.findAllByCategory_Id(id); }
    public Long getContLikes(Long id){
        return faqRepository.findById(id).get().getLikes();
    }
    public void setContLikes(Long id, Long likes){
        faqRepository.findById(id).get().setLikes(likes);
    }
    public Faq getOne(Long id) {
        return faqRepository.findById(id).get();
    }

    @ResponseBody
    public HttpEntity<byte[]> fileDownload(Long id) {
        File file = fileRepository.getOne(id);
        byte[] documentBody = file.getContent();
        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.parseMediaType(file.getContentType()));
        header.set(HttpHeaders.CONTENT_DISPOSITION,"attachment; filename=" + file.getFilename());
        header.setContentLength(documentBody.length);

        return new HttpEntity<byte[]>(documentBody, header);
    }
    
    public void like(Long id){
        Faq faq = faqRepository.getOne(id);
        faq.setLikes(faqRepository.getOne(id).getLikes()+1);
        faqRepository.save(faq);
    }
    
    public void unlike(Long id){
        Faq faq = faqRepository.getOne(id);
        faq.setUnlikes(faqRepository.getOne(id).getUnlikes()+1);
        faqRepository.save(faq);
    }

    public void status(Long id){
        Faq faq = faqRepository.getOne(id);
        faq.setStatus(true);
        faqRepository.save(faq);
    }
}
