package br.edu.ifrs.canoas.lds.webapp.domain.enumerated;

public enum Size {
	SMALL("Quase invisível"),
	MEDIUM("Acima do seu joelho"),
	BIG("Ele pode tocar seu rosto quando você está de pé"),
	BIGGER("Você vai precisar de uma casa maior");

	public String size;

	Size(String size) {
		this.size = size;
	}

	public String getSize() {
		return size;
	}
}
