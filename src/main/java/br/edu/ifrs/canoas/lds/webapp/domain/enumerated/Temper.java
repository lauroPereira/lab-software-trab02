package br.edu.ifrs.canoas.lds.webapp.domain.enumerated;

public enum Temper {
	QUIET("Mais quieto do que o seu sono (sem roncos)"),
	LOUD("Mais quieto do que o seu sono (com roncos)"),
	LOUDER("Ele vai latir aqui e lá"),
	LOUDEST("Mais barulhento que uma britadeira");

	public String valor;

	Temper(String temper) {
		this.valor = temper;
	}

	public String getTemper() {
		return valor;
	}
}
