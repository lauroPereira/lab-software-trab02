package br.edu.ifrs.canoas.lds.webapp.domain.enumerated;

public enum Status {
	WAITING("Aguardando"), LOST("Perdido"), FOUND("Achado"), ADOPTED("Adotado");

	public String status;

	Status(String valor) {
		status = valor;
	}

	public String getStatus() {
		return status;
	}
}
