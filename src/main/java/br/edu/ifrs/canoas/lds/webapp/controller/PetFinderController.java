package br.edu.ifrs.canoas.lds.webapp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/petfinder")
public class PetFinderController {

	@GetMapping("list")
	public ModelAndView listFound() {
		return new ModelAndView("/petfinder/list");
	}

}
