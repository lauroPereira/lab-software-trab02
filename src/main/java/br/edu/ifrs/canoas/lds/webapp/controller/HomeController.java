package br.edu.ifrs.canoas.lds.webapp.controller;

import br.edu.ifrs.canoas.lds.webapp.service.AdService;
import br.edu.ifrs.canoas.lds.webapp.service.FaqService;
import br.edu.ifrs.canoas.lds.webapp.service.NewsService;
import br.edu.ifrs.canoas.lds.webapp.service.PetService;
import org.springframework.security.config.annotation.authentication.configuration.EnableGlobalAuthentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import java.util.regex.Pattern;

/**
 * Created by rodrigo on 2/21/17.
 */
@Controller
public class HomeController {

    private final FaqService faqService;
    private final NewsService newsService;
    private final PetService petService;
    private final AdService adService;

    public HomeController(FaqService faqService, NewsService newsService, PetService petService, AdService adService) {
        this.faqService = faqService;
        this.newsService = newsService;
        this.petService = petService;
        this.adService = adService;
    }

    @GetMapping("/")
    public ModelAndView greetings(Model model) {
        model.addAttribute("faqs", faqService.list());
        model.addAttribute("faqs1", faqService.findAllByCategory(101L));
        model.addAttribute("faqs2", faqService.findAllByCategory(102L));
        model.addAttribute("faqs3", faqService.findAllByCategory(103L));
        model.addAttribute("faqs4", faqService.findAllByCategory(104L));
        model.addAttribute("faqs5", faqService.findAllByCategory(105L));
        model.addAttribute("news", newsService.findLastestNews());
        model.addAttribute("pets", petService.findLatestPetsForAdoption());
        model.addAttribute("ads", adService.findShuffleAds());
        return new ModelAndView("/index");
    }

    @GetMapping("ad/visit/{id}")
    public ModelAndView adVisit( @PathVariable String id){
        String url = adService.registryVisitor(new Long(id));

        //url = (Pattern.matches("http(s)?://.*", url))?"http://"+url:url;
        return new ModelAndView("redirect:" + "http://"+url);
    }
}
