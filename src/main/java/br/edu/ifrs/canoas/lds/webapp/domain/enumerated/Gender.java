package br.edu.ifrs.canoas.lds.webapp.domain.enumerated;

public enum Gender {
	MALE("É um príncipe!"), FEMALE("É uma princesa!");

	public String gender;

	Gender(String valor) {
		gender = valor;
	}

	public String getGender() {
		return gender;
	}
}
