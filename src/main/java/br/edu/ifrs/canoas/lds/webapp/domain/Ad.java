package br.edu.ifrs.canoas.lds.webapp.domain;

import br.edu.ifrs.canoas.lds.webapp.domain.enumerated.Plan;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
public class Ad {

    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    private String title;

    @Nullable
    private String subTitle;

    @NotNull
    private String content;

    @NotNull
    private String phone;

    @NotNull
    private String email;

    @NotNull
    private String url;

    @Nullable
    private Integer visitors;

    @OneToOne
    private File image;

    @NotNull
    private Date initDate;

    @Nullable
    private Date expDate;

    @Enumerated(EnumType.STRING)
    private Plan plan;

    public Ad() {}
    public Ad(Long id, String title, @Nullable String subTitle, String content, String phone, String email, String url, File image, Date initDate, @Nullable Date expDate, Plan plan, @Nullable Integer visitors) {
        this.id = id;
        this.title = title;
        this.subTitle = subTitle;
        this.content = content;
        this.phone = phone;
        this.email = email;
        this.url = url;
        this.image = image;
        this.initDate = initDate;
        this.expDate = expDate;
        this.plan = plan;
        this.visitors = visitors;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Nullable
    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(@Nullable String subTitle) {
        this.subTitle = subTitle;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public File getImage() {
        return image;
    }

    public void setImage(File image) {
        this.image = image;
    }

    public Date getInitDate() {
        return initDate;
    }

    public void setInitDate(Date initDate) {
        this.initDate = initDate;
    }

    @Nullable
    public Date getExpDate() {
        return expDate;
    }

    public void setExpDate(@Nullable Date expDate) {
        this.expDate = expDate;
    }

    public Plan getPlan() {
        return plan;
    }

    public void setPlan(Plan plan) {
        this.plan = plan;
    }

    @Nullable
    public Integer getVisitors() {
        return visitors;
    }

    public void setVisitors(@Nullable Integer visitors) {
        this.visitors = visitors;
    }

    @Override
    public String toString() {
        return "Ad{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", subTitle='" + subTitle + '\'' +
                ", content='" + content + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", url='" + url + '\'' +
                ", image=" + image +
                ", initDate=" + initDate +
                ", expDate=" + expDate +
                ", visitors=" + visitors +
                '}';
    }
}