package br.edu.ifrs.canoas.lds.webapp.config.auth;

import br.edu.ifrs.canoas.lds.webapp.domain.File;
import br.edu.ifrs.canoas.lds.webapp.domain.Role;
import br.edu.ifrs.canoas.lds.webapp.domain.User;
import br.edu.ifrs.canoas.lds.webapp.repository.FileRepository;
import br.edu.ifrs.canoas.lds.webapp.repository.RoleRepository;
import br.edu.ifrs.canoas.lds.webapp.repository.UserRepository;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionSignUp;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import static org.apache.commons.lang.RandomStringUtils.randomAlphabetic;

@Service
public class FacebookConnectionSignup implements ConnectionSignUp {

    private final UserRepository userRepository;
    private final FileRepository fileRepository;
    private final RoleRepository roleRepository;

    public FacebookConnectionSignup(UserRepository userRepository, FileRepository fileRepository, RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.fileRepository = fileRepository;
        this.roleRepository = roleRepository;
    }


    @Override
    public String execute(Connection<?> connection) {

        File file = new File();
        file.setUrl(connection.getImageUrl());
        file.setCreatedOn(new Date());
        fileRepository.save(file);

        Set<Role> roles = new HashSet<>();
        roles.add(roleRepository.findByRole("ROLE_FACEBOOK_USER"));

        User user = new User();
        user.setName(connection.getDisplayName());
        user.setUsername(connection.getDisplayName());
        user.setPicture(file);
        user.setFacebookProfileUrl(connection.getProfileUrl());
        user.setPassword(randomAlphabetic(8));
        user.setRoles(roles);
        userRepository.save(user);
        return user.getUsername();
    }
}