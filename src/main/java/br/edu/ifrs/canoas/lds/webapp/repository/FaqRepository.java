package br.edu.ifrs.canoas.lds.webapp.repository;
import br.edu.ifrs.canoas.lds.webapp.domain.Faq;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface FaqRepository extends JpaRepository<Faq, Long> {

	Optional<Faq> findByQuestion(String question);
	List<Faq> findAll();
	List<Faq> findAllById(Long id);
	List<Faq> findAllByStatus(boolean status);
	List<Faq> findAllByCategory_Id(Long id);

}