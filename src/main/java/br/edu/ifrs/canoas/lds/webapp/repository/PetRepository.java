package br.edu.ifrs.canoas.lds.webapp.repository;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import br.edu.ifrs.canoas.lds.webapp.domain.Pet;
import br.edu.ifrs.canoas.lds.webapp.domain.enumerated.Status;

@Repository
public interface PetRepository extends JpaRepository<Pet, Long>, JpaSpecificationExecutor<Pet> {
	Optional<Pet> findById(Long id);
    List<Pet> findAllByStatus(Status waiting);
    List<Pet> getAllByUserId(Long id);
    List<Pet> findTop3ByStatusOrderByExpiration(Status waiting);
    }
