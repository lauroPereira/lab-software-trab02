package br.edu.ifrs.canoas.lds.webapp.domain.filter;

import javax.validation.constraints.Size;

public class PetSearch {
    @Size(min = 3)
    private String advancedSearch;
    private boolean baby;
    private boolean adolescent;
    private boolean adult;
    private boolean ancient;
    private boolean dog;
    private boolean cat;
    private boolean other;
    private boolean male;
    private boolean female;
    private boolean small;
    private boolean medium;
    private boolean big;
    private boolean bigger;
    private boolean bold;
    private boolean hairy;
    private boolean mhairy;
    private boolean vhairy;
    private boolean bull;
    private boolean retired;
    private boolean notbull;
    private boolean good;
    private boolean determined;
    private boolean undetermined;

    public PetSearch() {
    }

    public String getAdvancedSearch() {
        return advancedSearch;
    }

    public void setAdvancedSearch(String advancedSearch) {
        this.advancedSearch = advancedSearch;
    }

    public boolean isBaby() {
        return baby;
    }

    public void setBaby(boolean baby) {
        this.baby = baby;
    }

    public boolean isAdolescent() {
        return adolescent;
    }

    public void setAdolescent(boolean adolescent) {
        this.adolescent = adolescent;
    }

    public boolean isAdult() {
        return adult;
    }

    public void setAdult(boolean adult) {
        this.adult = adult;
    }

    public boolean isAncient() {
        return ancient;
    }

    public void setAncient(boolean ancient) {
        this.ancient = ancient;
    }

    public boolean isDog() {
        return dog;
    }

    public void setDog(boolean dog) {
        this.dog = dog;
    }

    public boolean isCat() {
        return cat;
    }

    public void setCat(boolean cat) {
        this.cat = cat;
    }

    public boolean isOther() {
        return other;
    }

    public void setOther(boolean other) {
        this.other = other;
    }

    public boolean isMale() {
        return male;
    }

    public void setMale(boolean male) {
        this.male = male;
    }

    public boolean isFemale() {
        return female;
    }

    public void setFemale(boolean female) {
        this.female = female;
    }

    public boolean isSmall() {
        return small;
    }

    public void setSmall(boolean small) {
        this.small = small;
    }

    public boolean isMedium() {
        return medium;
    }

    public void setMedium(boolean medium) {
        this.medium = medium;
    }

    public boolean isBig() {
        return big;
    }

    public void setBig(boolean big) {
        this.big = big;
    }

    public boolean isBigger() {
        return bigger;
    }

    public void setBigger(boolean bigger) {
        this.bigger = bigger;
    }

    public boolean isBold() {
        return bold;
    }

    public void setBold(boolean bold) {
        this.bold = bold;
    }

    public boolean isHairy() {
        return hairy;
    }

    public void setHairy(boolean hairy) {
        this.hairy = hairy;
    }

    public boolean isMhairy() {
        return mhairy;
    }

    public void setMhairy(boolean mhairy) {
        this.mhairy = mhairy;
    }

    public boolean isVhairy() {
        return vhairy;
    }

    public void setVhairy(boolean vhairy) {
        this.vhairy = vhairy;
    }

    public boolean isNotbull() {
        return notbull;
    }

    public void setNotbull(boolean notbull) {
        this.notbull = notbull;
    }

    public boolean isBull() {
        return bull;
    }

    public void setBull(boolean bull) {
        this.bull = bull;
    }

    public boolean isRetired() {
        return retired;
    }

    public void setRetired(boolean retired) {
        this.retired = retired;
    }

    public boolean isNotBull() {
        return notbull;
    }

    public void setNotBull(boolean notbull) {
        this.notbull = notbull;
    }

    public boolean isGood() {
        return good;
    }

    public void setGood(boolean good) {
        this.good = good;
    }

    public boolean isDetermined() {
        return determined;
    }

    public void setDetermined(boolean determined) {
        this.determined = determined;
    }

    public boolean isUndetermined() {
        return undetermined;
    }

    public void setUndetermined(boolean undetermined) {
        this.undetermined = undetermined;
    }
}
