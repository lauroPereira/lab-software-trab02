/*
 * Copyright (c) 2017. Lauro Pereira
 */

package br.edu.ifrs.canoas.lds.webapp.domain.enumerated;

public enum Plan {
	MONTHLY("MONTHLY"), SEMESTER("SEMESTER"), YEARLY("YEARLY");

	public String plan;

	Plan(String valor) {
		plan = valor;
	}

	public String getPlan() {
		return plan;
	}

	public Integer getId() {
		switch (this.plan){
			case "MONTHLY":
				return 1;
			case "SEMESTER":
				return 2;
			case "YEARLY":
				return 3;
			default:
				return 0;
		}
	}
}
