package br.edu.ifrs.canoas.lds.webapp.domain.enumerated;

public enum Pelage {
	BOLD("Parece seu parente careca"),
	HAIRY("Você vai precisar de um pente, as vezes"),
	MHAIRY("Você precisa morar perto de um cabeleireiro"),
	VHAIRY("É recomendável deixar crianças longe risco de afogamento em pêlos");

	public String pelage;

	Pelage(String pelage) {
		this.pelage = pelage;
	}

	public String getPelage() {
		return pelage;
	}
}
