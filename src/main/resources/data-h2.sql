-- EMPLOYEE PHOTOS
insert into file(id, filename, content) values
(101, 'Teste',  FILE_READ('./src/main/resources/static/photos/face.jpg'));
insert into file(id, filename, content) values
(102, 'littleDog.jpg', FILE_READ('./src/main/resources/static/img-now/littleDog.jpg'));
insert into file(id, filename, content) values
(103, 'yellowDog.jpg', FILE_READ('./src/main/resources/static/img-now/yellowDog.jpg'));
insert into file(id, filename, content) values
(104, 'bg_ads2.jpg', FILE_READ('./src/main/resources/static/img-now/bg_ads2.jpg'));

--AUTHENTICATION
INSERT into user(id, username, password, name, email, experience, skill, active, picture_id) VALUES
(101, 'user', '$2a$10$Qji2/icFWIGGQEAv8bbwNuKGrSZyiUUPqE/0SNO2M.BpU.NA6xPhW', 'Master Yoda','petnationapp@gmail.com','Masters Unidentified Jedi, Garro, Qui-Gon Jinn', 'Deflect Force Lightning, Strategic Mastery, Acting Skills, Indomitable Will, Battle Meditation, Sensing Death And Force-aided Acrobatics.',  TRUE, 101);
INSERT into user(id, username, password, name, email, experience, skill, active, picture_id) VALUES
(102, 'admin', 'admin', 'Admin Yoda','petnationapp@gmail.com','Masters Unidentified Jedi, Garro, Qui-Gon Jinn', 'Deflect Force Lightning, Strategic Mastery, Acting Skills, Indomitable Will, Battle Meditation, Sensing Death And Force-aided Acrobatics.',  TRUE, 101);

--ROLES
insert into role(id, role) values
(1, 'ROLE_USER'),
(2, 'ROLE_ADMIN'),
(3, 'ROLE_FACEBOOK_USER');

-- USER_ROLES
insert into user_roles (user_id, roles_id) values
(101, 1),
(102,2);

-- NEWS
insert into  NEWS (ID ,AUTHOR ,CAPTION ,DATA_PUBLICATION ,DESCRIPTION ,TITLE)values
(101,'Dart', 'Feira de adocao em Canoas-RS', '2017-01-01', 'No proximo sabado, tera uma feira de adoções de animais de estimacao no centro da dicade de canoas, no parque da emancipacao.', '5 Feira de Adocao Canoense'),
(102,'Jedhi', 'Feira de adocao em Canoas-RS', '2017-02-01', 'No proximo sabado, tera uma feira de adoções de animais de estimacao no centro da dicade de canoas, no parque da emancipacao.', '6 Feira de Adocao Canoense'),
(103,'Sith', 'Feira de adocao em Canoas-RS', '2017-10-01', 'No proximo sabado, tera uma feira de adoções de animais de estimacao no centro da dicade de canoas, no parque da emancipacao.', '12 Feira de Adocao Canoense');

--CATEGORY
INSERT into category(id, category) VALUES
(101, 'Animais em condomínio'),
(102, 'Esterilização de Cães'),
(103, 'Esterilização de Gatos'),
(104, 'Leis de proteção animal'),
(105, 'Maus tratos aos animais');

--FAQ
insert into faq(id, question, answer, category_id,likes, unlikes, status, file_id)VALUES
(101,'teste pergunta', 'teste resposta', 101, 0, 0, 'TRUE', 102);


--PET
insert into pet(id, age, breed, description, expiration, gender, health, localization, name, pelage, register, size, species, status, temperament,user_id) values
(1001, 'ADOLESCENT', 'DETERMINED','Meme da internet que se tornou popular em 2013. Normalmente consiste em uma imagem de Shiba Inu acompanhada de textos multicoloridos em fonte Comic Sans. O texto, similar a uma reflexão pessoal, é deliberadamente escrito em uma forma inglesa incorreta', '2017-12-29 00:00:00.0','MALE','BULL','Canoas', 'Doge', 'MHAIRY', current_date, 'MEDIUM','DOG','WAITING','LOUD',101);
insert into pet(id, age, breed, description, expiration, gender, health, localization, name, pelage, register, size, species, status, temperament,user_id) values
(1002, 'BABY', 'UNDETERMINED', 'Eu sou uma doguinha','2017-12-29 00:00:00.0','FEMALE','BULL','Alvorada','Amy','HAIRY', current_date, 'MEDIUM','DOG','WAITING','LOUD',101);
insert into pet(id, age, breed, description, expiration, gender, health, localization, name, pelage, register, size, species, status, temperament,user_id) values
(1003, 'ADOLESCENT', 'DETERMINED','Meme da internet que se tornou popular em 2013. Normalmente consiste em uma imagem de Shiba Inu acompanhada de textos multicoloridos em fonte Comic Sans. O texto, similar a uma reflexão pessoal, é deliberadamente escrito em uma forma inglesa incorreta', '2017-12-29 00:00:00.0','MALE','BULL','Canoas', 'Doge', 'MHAIRY', current_date, 'MEDIUM','DOG','WAITING','LOUD',101);

--PET_PICTURES
insert into pet_pictures(pet_id, pictures_id) values (1001,101);
insert into pet_pictures(pet_id, pictures_id) values (1002,102);
insert into pet_pictures(pet_id, pictures_id) values (1003,103);

--AD
insert into AD (id, title, sub_title, content, phone, email, url, image_id, init_date, exp_date, plan, visitors)values
(1001, 'Dr Cat', 'O melhor lugar para seu Pet', 'Mencione o site petNation na ligação e ganhe a primeira consulta de graça.','3345-6789','contato@drcat.com','www.drpet.com.br',104,'2017-09-01 00:00:00.0','2018-02-01 00:00:00.0', 'SEMESTER', 0);
insert into AD (id, title, sub_title, content, phone, email, url, image_id, init_date, exp_date, plan, visitors)values
(1002, 'Doguinho', 'Seu amiguinho nas mãos de quem entende.', 'Ligue agora e receba um brinde especial','3948-5766','contato@doguinho.com','www.doguinho.com.br',103,'2017-10-01 00:00:00.0','2018-11-01 00:00:00.0', 'MONTHLY', 0);
insert into AD (id, title, sub_title, content, phone, email, url, image_id, init_date, exp_date, plan, visitors)values
(1003, 'Pet House', 'Tudo que seu pet precisa.', 'clique e descubra','1234-8844','contato@pethouse.com','www.google.com.br',102,'2017-10-01 00:00:00.0','2018-11-01 00:00:00.0', 'YEARLY', 0);