package br.edu.ifrs.canoas.lds.webapp.service;


import br.edu.ifrs.canoas.lds.webapp.domain.File;
import br.edu.ifrs.canoas.lds.webapp.domain.Pet;
import br.edu.ifrs.canoas.lds.webapp.domain.User;
import br.edu.ifrs.canoas.lds.webapp.domain.enumerated.*;
import br.edu.ifrs.canoas.lds.webapp.domain.filter.PetSearch;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.multipart.MultipartFile;

import java.lang.reflect.Array;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class PetServiceTest {

    private static final Long PET_ID = 1002L;
    private static final Long USER_ID = 101L;
    private static final String PET_NAME = "Amy";
    private static final String PET_DESCRIPTION = "Doguinha da Jojob";
    private static final String PET_LOCALIZATION = "Alvorada";
    private static final String IMG1_LOCALIZATION = "/static/img/dog_128.jpg";
    private static final String IMG2_LOCALIZATION = "/static/img/yellowDog.jpg";
    private static final Age PET_AGE = Age.BABY;
    private static final Breed PET_BREED = Breed.UNDETERMINED;
    private static final Temper PET_TEMPERAMENT = Temper.LOUD;
    private static final Gender PET_GENDER = Gender.FEMALE;
    private static final Health PET_HEALTH = Health.BULL;
    private static final Pelage PET_PELAGE = Pelage.HAIRY;
    private static final Size PET_SIZE = Size.MEDIUM;
    private static final Species PET_SPECIES = Species.DOG;
    private static final Status PET_STATUS = Status.WAITING;
    private static final String PET_JUSTIFICATION = "justificativa";
    private static final String PET_FEEDBACK = "Feedback";

    ClassLoader classLoader = getClass().getClassLoader();


    @Autowired
    private PetService petService;

    private Pet getPet() {
        Pet pet = new Pet();
        pet.setId(PET_ID);
        pet.setName(PET_NAME);
        pet.setDescription(PET_DESCRIPTION);
        pet.setLocalization(PET_LOCALIZATION);
        pet.setExpiration(new Date());
        pet.setAge(PET_AGE);
        pet.setBreed(PET_BREED);
        /*pet.setJustification(PET_JUSTIFICATION);
        pet.setFeedback(PET_FEEDBACK);*/
        pet.setTemperament(PET_TEMPERAMENT);
        pet.setGender(PET_GENDER);
        pet.setHealth(PET_HEALTH);
        pet.setPelage(PET_PELAGE);
        pet.setSize(PET_SIZE);
        pet.setSpecies(PET_SPECIES);
        pet.setStatus(PET_STATUS);
        pet.setUser(this.getUser());
        return pet;
    }

    private PetSearch getPetSearch() {
        PetSearch pet = new PetSearch();
        pet.setAdvancedSearch("amy");
        return pet;
    }

    private PetSearch getPetSearchList() {
        PetSearch pet = new PetSearch();
        pet.setAdvancedSearch("dog");
        return pet;
    }

    private Set<MultipartFile> getFiles() {
        URL url2 = this.getClass().getResource(IMG1_LOCALIZATION);
        URL url = this.getClass().getResource(IMG2_LOCALIZATION);
        MockMultipartFile mockMultipartFile = new MockMultipartFile(url.getFile(), url.getFile().getBytes());
        MockMultipartFile mockMultipartFile2 = new MockMultipartFile(url2.getFile(), url2.getFile().getBytes());
        Set<MultipartFile> multipartFiles = new HashSet<>();

        multipartFiles.add(mockMultipartFile);
        multipartFiles.add(mockMultipartFile2);
        return multipartFiles;
    }


    private User getUser() {
        User user = new User();
        user.setId(USER_ID);
        user.setUsername("yoda");
        user.setEmail("petnationapp@gmail.com");
        return user;
    }

    @Test
    public void the_getOne_should_retrieve_pet_by_id() throws Exception {
        assertThat(petService.getOne(this.getPet().getId()).getName().equals("Amy"));
        assertThat(petService.getOne(this.getPet().getId()).getDescription().equals("Doguinha da Jojob"));
        assertThat(petService.getOne(this.getPet().getId()).getLocalization().equals("Alvorada"));
    }

    @Test
    public void the_findBySearch_should_retrieve_a_list_with_one_pet_with_this_term() throws Exception {
        assertThat(petService.findBySearch(this.getPetSearch())).size().isEqualTo(1);
    }

    @Test //TODO testing issue #12
    public void the_send_message_method_with_null_objects_should_not_throws_exception() throws Exception {
        Pet pet = null;
        User user = null;
        petService.sendMessage(pet, user);
    }

    @Test //TODO testing issue #12
    public void the_send_message_method_with_null_object_attributes_should_not_throws_exception() throws Exception {
        Pet pet = new Pet();
        pet.setName(null);
        User user = new User();
        user.setName(null);
        user.setEmail(null);
        petService.sendMessage(pet, user);
    }

    @Test //TODO testing issue #12
    public void the_send_message_method_with_invalid_mail_address_should_not_throws_exception() throws Exception {
        User user = this.getUser();
        user.setEmail("emailinvalido");
        Pet pet = this.getPet();
        petService.sendMessage(pet, user);
    }

    @Test //TODO testing issue #12
    public void the_send_message_method_with_unexistent_mail_address_should_not_throws_exception() throws Exception {
        User user = this.getUser();
        user.setEmail("aokskddfije@gmail.com");
        Pet pet = this.getPet();
        petService.sendMessage(pet, user);
    }

    @Test //TODO testing issue #1
    public void the_save_method_must_have_all_pet_data() throws Exception {
        assertThat(petService.save(this.getPet(), this.getUser(), this.getFiles())).hasNoNullFieldsOrProperties();
    }

    @Test //TODO testing issue #10
    public void the_save_method_must_have_at_least_two_pictures() throws Exception {

        assertThat(this.getPet().getPictures().size()).isGreaterThanOrEqualTo(2);
    }

    @Test //TODO testing issue #13
    public void the_pet_register_must_have_deadline() throws Exception {
        Pet pet = petService.save(this.getPet(), this.getUser(), this.getFiles());

        assertThat(pet.getExpiration()).isNotNull();

    }

    @Test //TODO testing issue #16
    public void the_pet_register_must_have_localization() throws Exception {
        Pet pet = petService.save(this.getPet(), this.getUser(), this.getFiles());
        assertThat(pet.getLocalization()).isNotEmpty();
    }

    @Test //TODO testing issue #11
    public void the_pet_save_method_must_accept_only_jpg_or_png_for_pictures() throws Exception {
        Pet pet = petService.save(this.getPet(), this.getUser(), this.getFiles());
        Set<br.edu.ifrs.canoas.lds.webapp.domain.File> files = pet.getPictures();
        for (br.edu.ifrs.canoas.lds.webapp.domain.File fileSet : files) {
            assertThat(fileSet.getContentType()).contains("image/jpg", "image/png");
        }
    }

    @Test //TODO testing issue #14
    public void the_pet_must_have_viewcounter_for_logged_users() {
        // to implement
    }

    @Test //TODO testing issue #14
    public void the_pet_must_have_viewcounter_for_non_logged_users() {
        //to implement
    }

    @Test //TODO testing issue #15
    public void the_pet_must_have_his_status_changed_after_adoption_confirmed() {
        //to implement
    }

    @Test //TODO testing issue #18
    public void the_save_pet_method_must_be_about_a_different_pet_from_the_same_user() {
        // to implement
    }

    @Test //TODO testing issue #17
    public void every_user_can_have_more_than_one_pet_to_donate() {
        //to implement
        //testing
    }

    /*Andressa's Issues*/

    @Test //TODO testing issue #28 A busca de animais não retornará animais adotados ou desativados.
    public void search_must_returns_only_pets_with_status_waiting() throws Exception {
        ArrayList<Pet> pets = (ArrayList) petService.findBySearch(getPetSearchList());
        for (Pet pet : pets) {
            assertThat(pet.getStatus()).isEqualTo(Status.WAITING);
        }
    }

    @Test //TODO testing issue #4 A busca de animais para adoção sem filtro irá retornar todos os anúncios.
    public void verify_if_search_without_filters_return_all_pets() throws Exception {
        ArrayList<Pet> petsSearch = (ArrayList) petService.findBySearch(new PetSearch());
        ArrayList<Pet> petsAll = (ArrayList) petService.findAllByStatus();
        assertThat(petsSearch.size()).isSameAs(petsAll.size());
    }

    @Test
    //TODO testing issue #3 A busca de animais para adoção pode ser filtrada pelo tipo, sexo, idade, porte, pelagem e saúde do animal.
    public void verify_if_search_can_be_done_with_all_filters() throws Exception {
        PetSearch petSearch = new PetSearch();
        petSearch.setAdvancedSearch("dog");
        petSearch.setDog(true);
        petSearch.setFemale(true);
        petSearch.setBaby(true);
        petSearch.setUndetermined(true);
        petSearch.setBull(true);
        petSearch.setMedium(true);
        petSearch.setHairy(true);
        assertThat(petService.findBySearch(petSearch)).isNotNull();
    }

    @Test //TODO issue #34 A busca de animais considera apenas os dois primeiros termos inseridos.
    public void test_if_search_term_research_with_only_the_two_firsts_words() throws Exception {
        PetSearch petSearch = new PetSearch();
        petSearch.setAdvancedSearch("eu sou lalalala");
        assertThat(petService.findBySearch(petSearch).size()).isGreaterThanOrEqualTo(1);
    }

    @Test
    //TODO issue #5 - A busca de animais para adoção irá retornar uma lista com o nome, data do cadastro, breve descrição e uma foto do animal.
    public void verify_content_list_of_pets_researcheds() throws Exception {
        ArrayList<Pet> pets = (ArrayList) petService.findBySearch(getPetSearchList());
        for (Pet pet : pets) {
            assertThat(pet.getName()).isNotEmpty();
            assertThat(pet.getRegister()).isNotNull();
            assertThat(pet.getDescription()).isNotEmpty();
            //assertThat(pet.getPictures()).isNotEmpty();
        }
    }

    @Test
    //TODO testing issue #8 - A visualização de animais para adoção deve ter todos os dados do animal, incluindo contato do seu responsável.
    public void verify_fields_of_view_pet() throws Exception{
        Pet pet = petService.getOne(PET_ID);
        assertThat(pet.getName()).isNotEmpty();
        assertThat(pet.getGender()).isNotNull();
        assertThat(pet.getUser().getName()).isNotEmpty();
        assertThat(pet.getUser().getEmail()).isNotEmpty();
    }

    @Test //TODO testing issue #30 O termo de pesquisa deverá ser buscado no nome e na descrição do animal.
    public void test_if_the_search_term_will_be_researched_on_description_and_name_of_pet() throws Exception{
        ArrayList<Pet> pets = (ArrayList) petService.findBySearch(getPetSearchList());
        assertThat(pets.get(0).getDescription()).contains("dog");
        assertThat(pets.get(1).getName()).contains("Dog");
    }

    @Test //TODO testing issue #29 O termo de busca de animais deve conter no mínimo 3 caracteres.
    public void verify_if_term_of_search_pets_cannot_have_less_than_3_chars() throws Exception{
        PetSearch petSearch = new PetSearch();
        petSearch.setAdvancedSearch("xx");
        assertThat(petService.findBySearch(petSearch).size()).isGreaterThanOrEqualTo(3);
        //Verifica se a lista possui o mesmo tamanho da lista de todos os pets pois o método findBySearch retorna todos
        // os pets se a petSearch for null
    }

    /*End of Andressa's Issues*/


    /*@Test
    public void the_save_should_retrieve_pet() throws Exception{
    	MockMultipartFile file1 = new MockMultipartFile("file", "NameOfTheFile", "multipart/form-data", new FileInputStream( "path of the file"));
        MockMultipartFile file2 = new MockMultipartFile("file", "NameOfTheFile", "multipart/form-data", new FileInputStream( "path of the file"));
    	MultipartFile[] files = new MultipartFile[]{};
    	Pet pet = new Pet();
    	files[0] = file1;
    	files[1] = file2;

    	//assertThat(petService.save(getPet(), this.getUser(), files)).returns();
    }*/
}