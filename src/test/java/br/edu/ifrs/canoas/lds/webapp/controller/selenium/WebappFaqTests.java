package br.edu.ifrs.canoas.lds.webapp.controller.selenium;

import org.fluentlenium.adapter.junit.FluentTest;
import org.fluentlenium.core.hook.wait.Wait;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


import java.util.concurrent.TimeUnit;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Wait
public class WebappFaqTests extends FluentTest {

    @Value("${local.server.port}")
    int port;

    String getHost(String url) {
        return "http://localhost:" + port + "/" + url;
    }

    @Before
    public void setup() {
        goTo(getHost("/login"));
        $("#username").fill().with("admin");
        $("#password").fill().with("admin");
        $("#submit-login").click();
    }

    @Test
    public void verify_list_question(){
        goTo(getHost(""));
        await().atMost(2, TimeUnit.SECONDS).until(el("#navbarDropdownMenuPet")).present();
        $("#navbarDropdownMenuPet").click();
        await().atMost(2, TimeUnit.SECONDS).until(el("#btn-faq")).present();
        $("#btn-faq").click();
        await().atMost(2, TimeUnit.SECONDS).until(el("#cat1")).present();
        $("#cat1").click();
        await().atMost(2, TimeUnit.SECONDS).until(el("#cat2")).present();
        $("#cat2").click();
        await().atMost(2, TimeUnit.SECONDS).until(el("#cat3")).present();
        $("#cat3").click();
        await().atMost(2, TimeUnit.SECONDS).until(el("#cat4")).present();
        $("#cat4").click();
        await().atMost(2, TimeUnit.SECONDS).until(el("#cat5")).present();
        $("#cat5").click();
    }

    @Test
    public void verify_like_button(){
        goTo(getHost(""));
        await().atMost(2, TimeUnit.SECONDS).until(el("#navbarDropdownMenuPet")).present();
        $("#navbarDropdownMenuPet").click();
        await().atMost(2, TimeUnit.SECONDS).until(el("#btn-faq")).present();
        $("#btn-faq").click();
        await().atMost(2, TimeUnit.SECONDS).until(el("#cat1")).present();
        $("#cat1").click();
        await().atMost(2, TimeUnit.SECONDS).until(el("#like1")).present();
        $("#like1").click();
    }

    @Test
    public void verify_unLike_button(){
        goTo(getHost(""));
        await().atMost(2, TimeUnit.SECONDS).until(el("#navbarDropdownMenuPet")).present();
        $("#navbarDropdownMenuPet").click();
        await().atMost(2, TimeUnit.SECONDS).until(el("#btn-faq")).present();
        $("#btn-faq").click();
        await().atMost(2, TimeUnit.SECONDS).until(el("#cat1")).present();
        $("#cat1").click();
        await().atMost(2, TimeUnit.SECONDS).until(el("#unlike1")).present();
        $("#unlike1").click();
    }
    @Test
    public void verify_newQuestion_button(){
        goTo(getHost(""));
        await().atMost(2, TimeUnit.SECONDS).until(el("#navbarDropdownMenuPet")).present();
        $("#navbarDropdownMenuPet").click();
        await().atMost(2, TimeUnit.SECONDS).until(el("#btn-faq")).present();
        $("#btn-faq").click();
        await().atMost(2, TimeUnit.SECONDS).until(el("#newQuestion")).present();
        $("#newQuestion").click();
    }

    @Test
    public void verify_create_question(){
        goTo(getHost("/faq/newquestion"));
        await().atMost(2, TimeUnit.SECONDS).until(el("#category")).present();
        await().atMost(2, TimeUnit.SECONDS).until(el("#question")).present();
        $("#question").fill().with("O condomínio pode obrigar o uso de coleira nas dependências comuns do mesmo?");
        await().atMost(2, TimeUnit.SECONDS).until(el("#inputAnswer")).present();
        $("#inputAnswer").fill().with("O condomínio Pode, desde que esteja previsto no estatuto ou regimento interno do condomínio.");
        await().atMost(2, TimeUnit.SECONDS).until(el("#myFile")).present();
        await().atMost(2, TimeUnit.SECONDS).until(el("#enviar")).present();
        $("#enviar").click();
        await().atMost(2, TimeUnit.SECONDS).until(el("#cancel")).present();
    }

    @Test
    public void verify_aprove_question(){
        goTo(getHost("/faq/aprove"));
        await().atMost(2, TimeUnit.SECONDS).until(el("#status")).present();
        $("#status").click();
    }

    @After
    public void tearDown() {
        goTo(getHost("login?logout"));
    }
}
