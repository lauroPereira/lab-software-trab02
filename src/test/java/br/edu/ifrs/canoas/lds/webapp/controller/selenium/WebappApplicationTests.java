package br.edu.ifrs.canoas.lds.webapp.controller.selenium;

import org.fluentlenium.adapter.junit.FluentTest;
import org.fluentlenium.core.hook.wait.Wait;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.TimeUnit;

import static org.fluentlenium.assertj.FluentLeniumAssertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Wait
public class WebappApplicationTests extends FluentTest {

	@Value("${local.server.port}")
	int port;

	String getHost(String url) {
		return "http://localhost:" + port + "/" + url;
	}

	@org.junit.Before
	public void setup() {
		goTo(getHost(""));
		await().atMost(5, TimeUnit.SECONDS).until(el(".login-box-body")).present();
		$("#username").fill().with("user");
		$("#password").fill().with("user");
		$("#submit-login").click();
	}

	@Test
	public void abre_pagina_inicial_e_verifica_secoes_disponiveis() {
		goTo(getHost(""));
		await().atMost(6, TimeUnit.SECONDS).until(el("#pet-section")).present();
		assertThat($("#pet-donation")).hasText("Animais para adoção");
		assertThat($("#new-pets")).hasText("New Pets");

		await().atMost(6, TimeUnit.SECONDS).until(el("#news-section")).present();
		assertThat($("#Noticias")).hasText("Notícias");
		assertThat($("#news")).hasText("News");

		await().atMost(6, TimeUnit.SECONDS).until(el("#myCarousel")).present();
		assertThat($("#depoimento-nome")).hasText("Maria");
		assertThat($("#depoimento-descricao")).hasText(
				"Há quase um ano e meio enviei um e-mail para adotar um cãozinho com vocês. E foi a melhor coisa que eu fiz! Ele encheu nossa casa de amor, dengo, pelos e tranquilidade!! Para aqueles que ficam com o pé atrás de adotar um cão já adulto eu digo vai fundo!! Ele se encaixou na nossa família com uma facilidade que parecia que ele sempre esteve aqui. Aí no abrigo vcs chamavam ele de elfo, aqui em casa ganhou o nome de Spock!O cão mais fofo,carinhoso, bondoso e inteligente q já conheci! Minha sombrinha pela casa, companhia! Ele é super sociável e faz festa para qualquer um. Só tenho a agradecer por terem cuidado dele até o nosso encontro! Vou mandar umas fotinhos dele para vcs matarem a saudade! Beijos!");

		await().atMost(6, TimeUnit.SECONDS).until(el("#search-section")).present();
		assertThat($(".box-title")).hasText("Eu quero adotar um pet");
		assertThat($("label")).hasText("Selecione as opções de acordo com o perfil do pet desejado.");
	}

	@Test
	public void clica_no_pet_e_abre_pagina_do_detalhamento() {
		goTo(getHost(""));

		await().atMost(2, TimeUnit.SECONDS).until(el("#pet-section")).present();
		$("#pet-1").click();
		await().atMost(2, TimeUnit.SECONDS).until(el(".content-header")).present();
		assertThat($("h1")).hasText("Ver pet");
		await().atMost(2, TimeUnit.SECONDS).until(el(".content")).present();
		assertThat($("#pet-name")).hasText("Doge");
		assertThat($("#pet-health")).hasText("Mais forte que um touro");
		assertThat($("#pet-specie")).hasText("Cachorro");
		assertThat($("#pet-temperament")).hasText("Mais silencioso que seu sono (se você roncar)");
	}

	@Test
	public void clica_em_view_all_pets_e_abre_a_pagina_de_listagem() {
		goTo(getHost(""));
		await().atMost(2, TimeUnit.SECONDS).until(el("#pet-section")).present();
		$("#view-all").click();
		assertThat($("#pets")).hasText("Pets");
		assertThat($(".box-title")).hasText("Pets para adoção");
		assertThat($("#pet-name")).hasText("Doge");
		assertThat($("a")).hasText("Ver perfil completo");
		assertThat($("p")).hasText("Descrição");
	}

	@Test
	public void acessa_cadastro_pet() {
		goTo(getHost(""));
		// makeLogin();
		// await().atMost(2,
		// TimeUnit.SECONDS).until(el("#pet-section")).present();
		$("#petnation").click();
		assertThat($("#new-pet")).hasText("Cadastre um pet");
		$("#new-pet").click();

	}


	
	@After
	public void tearDown() {
		goTo(getHost("login?logout"));
	}
}
