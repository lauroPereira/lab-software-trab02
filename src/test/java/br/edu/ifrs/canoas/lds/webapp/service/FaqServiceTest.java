package br.edu.ifrs.canoas.lds.webapp.service;

import br.edu.ifrs.canoas.lds.webapp.domain.Category;
import br.edu.ifrs.canoas.lds.webapp.domain.Faq;
import br.edu.ifrs.canoas.lds.webapp.domain.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.multipart.MultipartFile;

import java.net.URL;
import java.util.HashSet;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class FaqServiceTest {
    private static final Long ID = 101L;
    private static final String QUESTION = "";
    private static final String ANSWER = "";
    /*private static final Category CATEGORY = 1;*/
    private static final Long LIKES = 0L;
    private static final Long UNLIKES = 0L;
    private static final boolean STATUS = false;
    private static final Long USERID = 101L;

    ClassLoader classLoader = getClass().getClassLoader();

    @Autowired
    private FaqService faqService;

    private Faq getFaq(){
        Faq faq = new Faq();
            faq.setId(ID);
            faq.setQuestion(QUESTION);
            faq.setAnswer(ANSWER);
            faq.setCategory(new Category());
            faq.setLikes(LIKES);
            faq.setUnLikes(UNLIKES);
            faq.setStatus(STATUS);
            return faq;
    }

    private User getUser() {
        User user = new User();
        user.setId(USERID);
        user.setUsername("yoda");
        user.setEmail("petnationapp@gmail.com");
        return user;
    }

    @Test
    public void the_getOne_should_retrieve_faq_by_id() throws Exception {
        assertThat(faqService.getOne(this.getFaq().getId()).getQuestion().equals("Teste Pergunta"));
        assertThat(faqService.getOne(this.getFaq().getId()).getAnswer().equals("Teste Resposta"));
    }

}


