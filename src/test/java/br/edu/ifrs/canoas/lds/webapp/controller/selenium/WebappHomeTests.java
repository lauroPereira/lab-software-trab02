package br.edu.ifrs.canoas.lds.webapp.controller.selenium;

import org.fluentlenium.adapter.junit.FluentTest;
import org.fluentlenium.core.hook.wait.Wait;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import java.util.concurrent.TimeUnit;
import static org.fluentlenium.assertj.FluentLeniumAssertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Wait
public class WebappHomeTests extends FluentTest {

    @Value("${local.server.port}")
    int port;

    String getHost(String url) {
        return "http://localhost:" + port + "/" + url;
    }

    //Todo: issue 81
    @Test
    public void mostra_noticias_na_tela_inicial() {
        goTo(getHost(""));
        await().atMost(2, TimeUnit.SECONDS).until(el(".section-news")).present();
        assertThat($("#news-title")).hasText("12 Feira de Adocao Canoense");
        assertThat($("#news-title")).hasText("6 Feira de Adocao Canoense");
    }

    //Todo: issue 80
    @Test
    public void clica_em_mais_noticias_e_abre_a_pagina_de_listagem() {
        goTo(getHost(""));
        await().atMost(2, TimeUnit.SECONDS).until(el(".section-news")).present();
        $("#list-new").click();
        assertThat($(".news-title")).hasText("5 Feira de Adocao Canoense");
    }

    //Todo: issue 74
    @Test
    public void acessa_detalhe_pet() {
        goTo(getHost(""));
        await().atMost(2, TimeUnit.SECONDS).until(el(".section-adopt")).present();
        $("#btn-adopt").click();
        assertThat($(".title")).hasText("Sobre mim");
    }

    //Todo: issue 73
    @Test
    public void mostra_anuncios() {
        goTo(getHost(""));
        await().atMost(2, TimeUnit.SECONDS).until(el(".section-help")).present();
        assertThat($(".title")).hasText("Doguinho");
    }

    //Todo: issue 72
    @Test
    public void mostra_animais_para_adocao() {
        goTo(getHost(""));
        await().atMost(2, TimeUnit.SECONDS).until(el(".section-adopt")).present();
        assertThat($(".title")).hasText("Doge");
        assertThat($(".title")).hasText("Amy");
    }

    //Todo: issue 71
    @Test
    public void mostra_foto_e_nome_do_animal() {
        goTo(getHost(""));
        await().atMost(2, TimeUnit.SECONDS).until(el(".section-adopt")).present();
        assertThat($(".section-adopt")).hasClass(".img-raised");
        assertThat($(".title")).hasText("Doge");
    }

    //Todo: issue 70
    @Test
    public void mostra_animais_com_foto() {
        goTo(getHost(""));
        await().atMost(2, TimeUnit.SECONDS).until(el(".section-adopt")).present();
        assertThat($(".section-adopt")).hasClass(".img-raised");
    }

    //Todo: issue 19
    @Test
    public void acessa_pagina_listagem_animais() {
        goTo(getHost(""));
        await().atMost(2, TimeUnit.SECONDS).until(el(".section-adopt")).present();
        $("#mais-pets").click();
        assertThat($("pet-for-adopt")).hasText("Pets para adoção");
    }

    //Todo: issue 51
    @Test
    public void abre_pagina_inicial_e_verifica_secoes_disponiveis() {
        goTo(getHost(""));
        await().atMost(2, TimeUnit.SECONDS).until(el(".section-news")).present();
        assertThat($("#news-title")).hasText("12 Feira de Adocao Canoense");

        await().atMost(2, TimeUnit.SECONDS).until(el(".section-adopt")).present();
        assertThat($(".title")).hasText("Está procurando companhia?");

        await().atMost(2, TimeUnit.SECONDS).until(el(".section-faq")).present();
        assertThat($(".title")).hasText("Perguntas frequentes");

        await().atMost(2, TimeUnit.SECONDS).until(el(".section-help")).present();
        assertThat($(".title")).hasText("Quer nos ajudar?");
    }
}
