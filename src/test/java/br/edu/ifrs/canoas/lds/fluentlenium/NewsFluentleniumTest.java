package br.edu.ifrs.canoas.lds.fluentlenium;

import org.fluentlenium.adapter.junit.FluentTest;
import org.fluentlenium.core.hook.wait.Wait;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
@Wait
public class NewsFluentleniumTest extends FluentTest{

	@Value("${local.server.port}") int port;
	String getHost(){return "http://localhost:"+ port+ "/protot/index2.html";}
	
	@Test
    public void news_test (){
		//Vai para http://localhost:"+ port+ "/protot/index2.html
        goTo(getHost());
        
	}
}
