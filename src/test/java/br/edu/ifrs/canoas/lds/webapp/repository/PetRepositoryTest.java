package br.edu.ifrs.canoas.lds.webapp.repository;

import br.edu.ifrs.canoas.lds.webapp.domain.enumerated.Status;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class PetRepositoryTest {

    @Autowired
    private PetRepository petRepository;

    @Test
    public void test_that_find_by_id_returns_the_pet() throws Exception {
        assertThat(petRepository.findById(-1L)).isEmpty();
        assertThat(petRepository.findById(1001L)).isNotEmpty();
    }

    @Test
    public void test_that_find_by_status_returns_list_of_pets() throws Exception {
        assertThat(petRepository.findAllByStatus(Status.WAITING)).isNotEmpty();
        assertThat(petRepository.findAllByStatus(Status.ADOPTED)).isEmpty();
        assertThat(petRepository.findAllByStatus(Status.FOUND)).isEmpty();
        assertThat(petRepository.findAllByStatus(Status.LOST)).isEmpty();
    }

    @Test
    public void test_that_get_by_userId_returns_list_of_pets() throws Exception {
        assertThat(petRepository.getAllByUserId(101L)).isNotEmpty();
    }
    
    @Ignore
    public void return_top_3_pets() throws Exception {
        //assertThat(petRepository.findTop3ByOrderByExpiration()).isNotEmpty();
    }



}