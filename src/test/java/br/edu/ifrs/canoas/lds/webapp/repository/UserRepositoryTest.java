package br.edu.ifrs.canoas.lds.webapp.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.*;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;


    @Test
    public void test_that_find_by_username_returns_the_user() throws Exception {
        assertThat(userRepository.findByUsernameIgnoreCase("user")).isNotEmpty();
        assertThat(userRepository.findByUsernameIgnoreCase("USER")).isNotEmpty();

        assertThat(userRepository.findByUsernameIgnoreCase("user that doest not exits")).isEmpty();
        assertThat(userRepository.findByUsernameIgnoreCase(null)).isEmpty();
    }

}