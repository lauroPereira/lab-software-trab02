package br.edu.ifrs.canoas.lds.webapp.controller.selenium;

import org.fluentlenium.adapter.junit.FluentTest;
import org.fluentlenium.core.hook.wait.Wait;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import java.util.concurrent.TimeUnit;
import static org.fluentlenium.assertj.FluentLeniumAssertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Wait
public class WebappPetFinderTests extends FluentTest {
	
	@Value("${local.server.port}")
	int port;

	String getHost(String url) {
		return "http://localhost:" + port + "/" + url;
	}

	@org.junit.Before
	public void setup() {
		goTo(getHost(""));
		await().atMost(5, TimeUnit.SECONDS).until(el(".login-box-body")).present();
		$("#username").fill().with("user");
		$("#password").fill().with("user");
		$("#submit-login").click();
	}
	
	@Test
	public void verify_content_list_petfinder() {
		// issue #69 - Listagem de Pets Achados e Perdido, terá nome, se foi achado ou
		// perdido, espécie, data de entrada no site, a ultima localização onde foi
		// visto ou encontrado.
		goTo(getHost("petfinder/list"));
		await().atMost(2, TimeUnit.SECONDS).until(el("#pet-photo")).present();
		await().atMost(2, TimeUnit.SECONDS).until(el("#pet-name")).present();
		assertThat($("#pet-name")).hasText("Doge");
		await().atMost(2, TimeUnit.SECONDS).until(el("#pet-status")).present();
		assertThat($("#pet-status")).hasText("Lost");
		await().atMost(2, TimeUnit.SECONDS).until(el("#pet-date")).present();
		assertThat($("#pet-date")).hasText("21/09/2017");
		await().atMost(2, TimeUnit.SECONDS).until(el("#pet-location")).present();
		assertThat($("#pet-location")).hasText("Canoas/RS");
		await().atMost(2, TimeUnit.SECONDS).until(el("#pet-description")).present();
		assertThat($("#pet-description")).hasText(
				"O cao (Canis lupus familiaris[1]), no Brasil tambem chamado de cachorro, e um mamifero canideo, subespecie do lobo, e talvez o mais antigo animal domesticado pelo ser humano. Teorias postulam que surgiu do lobo cinzento no continente asiatico ha mais de 100 000 anos.");
	}
	
	@Test
	public void verify_button_new_pet() {
		// issue #82 - Habilitar cadastro comum de Pets para achados, perdidos e adoções
		goTo(getHost("petfinder/list"));
		await().atMost(2, TimeUnit.SECONDS).until(el("#pet-new")).present();
		assertThat($("#pet-new")).hasText("Cadastre um pet");
		$("#pet-new").click();		
	}
	
	@Test
	public void verify_access_full_pet_perfil() {
		// issue #76 - Exibir perfil completo do Pet achado ou perdido.
		goTo(getHost("petfinder/list"));
		await().atMost(2, TimeUnit.SECONDS).until(el("#btn-view-pet-profile")).present();
		assertThat($("#btn-view-pet-profile")).hasText("Ver perfil completo");
		$("#btn-view-pet-profile").click();
	}
	
	@After
	public void tearDown() {
		goTo(getHost("login?logout"));
	}

}
