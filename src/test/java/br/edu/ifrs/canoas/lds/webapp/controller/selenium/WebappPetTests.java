package br.edu.ifrs.canoas.lds.webapp.controller.selenium;

import org.fluentlenium.adapter.junit.FluentTest;
import org.fluentlenium.core.hook.wait.Wait;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import static org.fluentlenium.assertj.FluentLeniumAssertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Wait
public class WebappPetTests extends FluentTest {

	@Value("${local.server.port}")
	int port;

	String getHost(String url) {
		return "http://localhost:" + port + "/" + url;
	}

	@Before
	public void setup() {
		goTo(getHost("login"));
		$("#username").fill().with("admin");
		$("#password").fill().with("admin");
		$("#submit-login").click();
	}

	@Test
	public void open_pet_view_page_and_click_to_mark(){
		await().atMost(2, TimeUnit.SECONDS).until(el("#pet-section")).present();
		$("#pet-1").click();
		await().atMost(2, TimeUnit.SECONDS).until(el(".content-header")).present();
		assertThat($("#mark-button")).hasText("Marcar anúncio para remoção");
		$("#mark-button").click();
		await().atMost(2, TimeUnit.SECONDS).until(el(".content-header")).present();
		assertThat($("#justification")).hasText("Justificativa");
	}
	
	@Test
	public void open_mark_page_fill_justification_with_four_letter(){
		await().atMost(2, TimeUnit.SECONDS).until(el("#pet-section")).present();
		$("#pet-1").click();
		await().atMost(2, TimeUnit.SECONDS).until(el(".content-header")).present();
		assertThat($("#mark-button")).hasText("Marcar anúncio para remoção");
		$("#mark-button").click();
		await().atMost(2, TimeUnit.SECONDS).until(el(".content-header")).present();
		assertThat($("#justification")).hasText("Justificativa");
		$("#justificationinput").fill().with("Fake");
		$("#mark-button").click();
		await().atMost(2, TimeUnit.SECONDS).until(el(".callout")).present();
		assertThat($(".callout")).hasText("Justificativa muito curta.");
	}
	
	@Test
	public void open_mark_page_submit_without_justification(){
		await().atMost(2, TimeUnit.SECONDS).until(el("#pet-section")).present();
		$("#pet-1").click();
		await().atMost(2, TimeUnit.SECONDS).until(el(".content-header")).present();
		assertThat($("#mark-button")).hasText("Marcar anúncio para remoção");
		$("#mark-button").click();
		await().atMost(2, TimeUnit.SECONDS).until(el(".content-header")).present();
		assertThat($("#justification")).hasText("Justificativa");
		$("#mark-button").click();
		await().atMost(2, TimeUnit.SECONDS).until(el(".callout")).present();
		assertThat($(".callout")).hasText("Justificativa muito curta.");
	}

	/*Andressa's Issues*/
	@Test //TODO testing issue #5 - A busca de animais para adoção irá retornar uma lista com o nome, data do cadastro, breve descrição e uma foto do animal.
    public void verify_content_list_of_pets_researcheds() {
	    DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		goTo(getHost("pet/listAll"));
		await().atMost(2, TimeUnit.SECONDS).until(el("#pet-photo")).present();
		assertThat($("#pet-name")).hasText("Amy");
		assertThat($("#pet-date")).hasText(df.format(new Date()));
		assertThat($("#pet-description")).hasText("Eu sou uma doguinha");
		assertThat($("#pet-for-adopt")).hasText("Pets para adoção");
	}

	@Test //TODO testing issue #3 - A busca de animais para adoção pode ser filtrada pelo tipo, sexo, idade, porte, pelagem e saúde do animal.
	public void verify_if_search_can_be_done_with_all_filters() {
		goTo(getHost("pet/search"));
		await().atMost(2, TimeUnit.SECONDS).until(el("#input-pet-search-advanced")).present();
		($("#pet-dog")).fillSelect();
		($("#pet-female")).fillSelect();
		($("#pet-medium")).fillSelect();
        ($("#pet-baby")).fillSelect();
		$("#pet-search-submit").click();
        await().atMost(2, TimeUnit.SECONDS).until(el("#pet-for-adopt")).present();
        assertThat($("#pet-for-adopt")).hasText("Pets para adoção");
	}

	@Test //TODO testing issue #8 - A visualização de animais para adoção deve ter todos os dados do animal, incluindo contato do seu responsável.
	public void verify_fields_of_view_pet() {
		goTo(getHost("pet/view/1001"));
		assertThat($("#pet-name")).hasText("Doge");
		assertThat($("#pet-species")).hasText("É um totó");
		assertThat($("#pet-gender")).hasText("É um príncipe");
		assertThat($("#pet-responsible-contact")).hasText("Master Yoda");
		assertThat($("#pet-responsible-email")).hasText("petnationapp@gmail.com");
	}

	@Test //TODO testing issue #4 A busca de animais para adoção sem filtro irá retornar todos os anúncios.
    public void verify_if_search_without_filters_return_all_pets(){
        goTo(getHost("pet/search"));
        $("#pet-search-submit").click();
        await().atMost(3, TimeUnit.SECONDS).until(el(".team")).present();
        assertThat($(".col-md-4").count()).isEqualTo(3);
    }

    @Test //TODO testing issue #29 O termo de busca de animais deve conter no mínimo 3 caracteres.
    public void verify_if_term_of_search_pets_cannot_have_less_than_3_chars(){
        goTo(getHost("pet/search"));
        $("#input-pet-search-advanced").fill().with("aa");
        $("#pet-search-submit").click();
        ($("#pet-dog")).fillSelect();
        assertThat($("#pet-for-adopt")).hasText("Pets para adoção");
        //aqui ele vai para a página de list pois o método retorna todos os pets se não houver filtro indicado
    }

    @Test //TODO testing issue #30 O termo de pesquisa deverá ser buscado no nome e na descrição do animal.
    public void test_if_the_search_term_will_be_researched_on_description_and_name_of_pet() {
        goTo(getHost("pet/search"));
        $("#input-pet-search-advanced").fill().with("dog");
        $("#pet-search-submit").click();
        assertThat($("#pet-name")).hasText("Doge");
        assertThat($("#pet-description")).hasText("Eu sou uma doguinha");
    }

    @Test  //TODO testing issue #34 A busca de animais considera apenas os dois primeiros termos inseridos.
    public void test_if_search_term_research_with_only_the_two_firsts_words() {
        goTo(getHost("pet/search"));
        $("#input-pet-search-advanced").fill().with("eu sou xxx");
        $("#pet-search-submit").click();
        assertThat($("#pet-name")).hasText("Amy");
        assertThat($("#pet-description")).hasText("Eu sou uma doguinha");
    }

    @Test //TODO testing issue #28 A busca de animais não retornará animais adotados ou desativados.
    public void search_must_returns_only_pets_with_status_waiting(){
        goTo(getHost("pet/listAll"));
        assertThat($(".col-md-4").count()).isEqualTo(3);
        goTo(getHost("pet/view/1002"));
        $("#want-adopt-pet").click();
        goTo(getHost("pet/search"));
        $("#pet-search-submit").click();
        assertThat($(".col-md-4").count()).isEqualTo(2);
    }
	/*End of Andressa's Issues*/

    @After
	public void tearDown() {
		goTo(getHost("login?logout"));
	}
}
