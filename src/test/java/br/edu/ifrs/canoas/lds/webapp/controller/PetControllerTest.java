package br.edu.ifrs.canoas.lds.webapp.controller;

import br.edu.ifrs.canoas.lds.webapp.config.Messages;
import br.edu.ifrs.canoas.lds.webapp.domain.Pet;
import br.edu.ifrs.canoas.lds.webapp.service.PetService;
import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasProperty;
import static org.mockito.BDDMockito.given;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(PetController.class)
public class PetControllerTest extends BaseControllerTest {
    private static final String URL = "/pet/";
    private static final Long PET_ID = 1001L;
    private static final String PET_NAME = "Doge";


    @MockBean
    Messages messages;
    @MockBean
    PetService petService;

    Pet pet;
    List<Pet> pets = new ArrayList<>();

    @Before
    public void setUp(){
        pet = new Pet();
        pet.setId(PET_ID);
        pet.setName(PET_NAME);
        pets.add(pet);
    }

    @Test
    public void view_list_of_pets() throws Exception {
        given(this.petService.findAllByStatus()).willReturn(pets);

        this.mvc.perform(get(URL+"listAll")
                .with(user(userDetails))
                .accept(MediaType.TEXT_HTML)
        )
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/html;charset=UTF-8"))
                .andExpect(view().name("/pet/list"));
                //.andExpect(model().attribute("pets",pets));
    }
}