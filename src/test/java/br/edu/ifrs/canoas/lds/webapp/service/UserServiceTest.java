package br.edu.ifrs.canoas.lds.webapp.service;

import br.edu.ifrs.canoas.lds.webapp.domain.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UserServiceTest {

    private static final Long USER_ID = 101L;
    private static final String USER_MAIL = "user@user.user";
    private static final String USER = "user";

    @Autowired
    private UserService userService;

    @Test
    public void the_getOne_shoud_retrieve_user_by_id () throws Exception {
        assertThat(userService.getOne(this.getUser()).getUsername()).isEqualTo(USER);
        assertThat(userService.getOne(this.getUser()).getEmail()).isEqualTo("petnationapp@gmail.com");

        assertThat(userService.getOne(this.getUser()).getRoles())
                .extracting("role").contains("ROLE_USER");

        assertThat(userService.getOne(null)).isNull();

    }

    @Test
    public void save_user_only_with_name_and_mail() throws Exception {
        User savedUser = userService.save(this.getUser());
        assertThat(savedUser.getEmail()).isEqualTo(USER_MAIL);

        assertThat(savedUser.getUsername()).isEqualTo(USER);
        assertThat(savedUser.getSkill()).isNull();
        assertThat(savedUser.getExperience()).isNull();

        assertThat(savedUser.getPicture().getContent()).isNotEmpty();
    }

    private User getUser(){
        User user = new User();
        user.setId(USER_ID);
        user.setName(USER);
        user.setEmail(USER_MAIL);
        return user;
    }

}