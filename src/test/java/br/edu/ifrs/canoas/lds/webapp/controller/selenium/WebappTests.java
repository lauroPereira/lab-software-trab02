package br.edu.ifrs.canoas.lds.webapp.controller.selenium;

import org.fluentlenium.adapter.junit.FluentTest;
import org.fluentlenium.core.hook.wait.Wait;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.TimeUnit;

import static org.fluentlenium.assertj.FluentLeniumAssertions.assertThat;

/**
 * Created by victorjunior on 12/10/17.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Wait
public class WebappTests extends FluentTest{

    private static final String IMG1_LOCALIZATION = "/static/img/dog_128.jpg";
    private static final String IMG2_LOCALIZATION = "/static/img/yellowDog.jpg";

    @Value("${local.server.port}")
    int port;

    String getHost(String url) {
        return "http://localhost:" + port + "/" + url;
    }

    @Before
    public void setup() {
        goTo(getHost(""));
        await().atMost(5, TimeUnit.SECONDS).until(el("#faq")).present();
        /*$("#username").fill().with("user");
        $("#password").fill().with("user");
        $("#submit-login").click();*/
    }

    public void login() {
        await().atMost(5, TimeUnit.SECONDS).until(el("form"));
        $("#username").fill().with("user");
        $("#password").fill().with("user");
        $("#submit-login").click();
    }

    @Test
    public void abre_pagina_inicial_e_seleciona_doar_pet() {
        goTo(getHost(""));
        $("#navbarDropdownMenuPet").click();
        $("#newPet").click();
        login();
        await().atMost(5, TimeUnit.SECONDS).until(el("#submit-pet"));
        $("#name").fill().with("Animalzinho");
        String name = $("#name").value();
        $("#localization").fill().with("Porto Alegre");
        $("#species").fillSelect().withIndex(1);
        $("#gender").fillSelect().withIndex(1);
        $("#age").fillSelect().withIndex(2);
        $("#size").fillSelect().withIndex(1);
        $("#pelage").fillSelect().withIndex(2);
        $("#breed").fillSelect().withIndex(0);
        $("#temper").fillSelect().withIndex(1);
        $("#health").fillSelect().withIndex(2);
        $("#status").fillSelect().withIndex(1);
        $("#expiration").fill().with("30/10/2017");
        $("#description").fill().with("Teste de animal");
        $("#myFile").fill().with(IMG1_LOCALIZATION,IMG2_LOCALIZATION);
        $("#submit-pet").click();
        assertThat($(".team-player").present());

    }


}
