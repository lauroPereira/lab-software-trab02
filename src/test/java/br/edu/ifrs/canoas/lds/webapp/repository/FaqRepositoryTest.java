package br.edu.ifrs.canoas.lds.webapp.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class FaqRepositoryTest {

    @Autowired
    private FaqRepository faqRepository;

    @Test
    public void test_that_find_by_id_returns_the_faq() throws Exception {
        assertThat(faqRepository.findById(-1L)).isEmpty();
        assertThat(faqRepository.findById(101L)).isNotEmpty();
    }

    @Test
    public void test_that_get_by_faqId_returns_list_of_faqs() throws Exception {
        assertThat(faqRepository.findAllById(101L)).isNotEmpty();
    }

}
